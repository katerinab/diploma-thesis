import copy
import warnings
from pathlib import Path
from typing import Dict, Tuple, Optional

import graphviz
import matplotlib.pyplot as plt
import neat
import numpy as np

# partially adapted from: https://github.com/CodeReclaimers/neat-python/blob/master/examples/xor/visualize.py
from utils.hyperneat_reporter import Reporter, Stats


def plot_stats(statistics, path: Path, ylog: bool = False, view: bool = False, subtitle: str = "",
               current_state_name='average'):
    """ Plots the population's average and best fitness. """
    if plt is None:
        warnings.warn("This display is not available due to a missing optional dependency (matplotlib)")
        return

    generations = range(statistics.generation + 1)
    best_fitness = [c.fitness_best for c in statistics.stats.generation_stats]
    avg_fitness = np.array(statistics.stats.get_mean())
    stdev_fitness = np.array(statistics.stats.get_std())

    fig = plt.figure()
    plt.plot(generations, avg_fitness, 'b-', label=current_state_name)
    plt.plot(generations, avg_fitness - stdev_fitness, 'g-.', label="-1 std")
    plt.plot(generations, avg_fitness + stdev_fitness, 'g-.', label="+1 std")
    plt.plot(generations, best_fitness, 'r-', label="best")
    if current_state_name == 'average':
        plt.title("Population's average and best fitness\n" + subtitle)
    else:
        plt.title("DQN solution progress\n" + subtitle)
    plt.xlabel("Generations" if current_state_name == 'average' else "progress each x time steps")
    plt.ylabel("Fitness")
    plt.grid()
    plt.legend(loc="best")
    if ylog:
        plt.gca().set_yscale('symlog')

    plt.savefig(path)
    if view:
        plt.show()

    plt.close()


def plot_all_stats(data: Dict[str, Stats], path: Optional[Path], shape: Tuple[int, int], env_name: str, pop_size: int,
                   show: bool = False, is_dqn: bool = True):
    "Plot training stats of several runs in one plot"
    fig = plt.figure(figsize=(5 * shape[1], 3 * shape[0]))
    i = 0
    fs = 15
    fs_large=15
    for exp_name, statistics in data.items():
        is_dqn = "DQN" in env_name
        i += 1
        ax = plt.subplot(shape[0], shape[1], i)
        ax.tick_params(axis='both', which='major', labelsize=fs)
        ax.tick_params(axis='both', which='minor', labelsize=fs)
        generations = range(len(statistics.generation_stats))
        best_fitness = [c.fitness_best for c in statistics.generation_stats]
        avg_fitness = np.array(statistics.get_mean())
        stdev_fitness = np.array(statistics.get_std())
        plt.plot(generations, avg_fitness, 'b-', label="current" if is_dqn else "avg ind")
        # plt.plot(generations, avg_fitness - stdev_fitness, 'g-.', label="-1 std")
        # plt.plot(generations, avg_fitness + stdev_fitness, 'g-.', label="+1 std")
        plt.plot(generations, best_fitness, 'r-', label="best" if is_dqn else "best ind")
        plt.xlabel("Logging step" if is_dqn else "Generation", fontsize=fs)
        plt.ylabel("Fitness", fontsize=fs)
        plt.ylim((-200, -60))
        plt.grid()
        plt.legend(loc="best", fontsize=fs)
        # plt.title(exp_name.replace("_", " "), fontsize=fs)
    plt.suptitle(env_name, fontsize=fs_large)
    plt.tight_layout()
    if path is not None:
        plt.savefig(path)
    if show:
        plt.show()

    plt.close()


def plot_species(statistics: Reporter, path: Path, view: bool = False):
    """ Visualizes speciation throughout evolution. """
    if plt is None:
        warnings.warn("This display is not available due to a missing optional dependency (matplotlib)")
        return

    species_sizes = statistics.get_species_sizes()
    num_generations = len(species_sizes)
    curves = np.array(species_sizes).T

    fig, ax = plt.subplots()
    ax.stackplot(range(num_generations), *curves)

    plt.title("Speciation")
    plt.ylabel("Size per Species")
    plt.xlabel("Generations")

    plt.savefig(path)

    if view:
        plt.show()

    plt.close()


def draw_net(config: neat.config.Config, genome: neat.DefaultGenome, path: Path, format: str,
             title: str,
             view=False, node_names=None, show_disabled=True, prune_unused=False,
             node_colors=None, weight_out=True):
    """ Receives a genome and draws a neural network with arbitrary topology. """
    # Attributes for network nodes.
    if graphviz is None:
        warnings.warn("This display is not available due to a missing optional dependency (graphviz)")
        return

    if node_names is None:
        node_names = {}

    assert type(node_names) is dict

    if node_colors is None:
        node_colors = {}

    assert type(node_colors) is dict

    node_attrs = {
        'shape': 'circle',
        'fontsize': '14',
        'height': '0.9',
        'width': '0.9',
        'fixedsize':'true'
    }

    dot = graphviz.Digraph(format=format, node_attr=node_attrs)
    dot.attr(label=title, labelloc='t')

    inputs = set()
    for k in [-1,-2,-3,-4]:
        inputs.add(k)
        input_labels = ['n1.l', 'n1.n', 'n2.l', 'n2.n']
        label = node_names.get(k, f'{input_labels[k * (-1) - 1]}')
        name = node_names.get(k, str(k))
        input_attrs = {'style': 'filled', 'shape': 'box', 'fillcolor': node_colors.get(k, 'lightgray'),
                       'height':'0.2', 'width':'0.2', 'fixedsize':'false'}
        dot.node(name, label=label, _attributes=input_attrs)

    outputs = set()
    for k in [0,1]:
        outputs.add(k)
        output_labels = ['weight' if weight_out else 'bias', 'enabled']
        label = node_names.get(k,
                               f'{output_labels[k]} \n{genome.nodes[k].activation}\nb={genome.nodes[k].bias:.2f}')
        name = node_names.get(k, str(k))
        node_attrs = {'style': 'filled', 'fillcolor': node_colors.get(k, 'lightblue')}

        dot.node(name, label=label, _attributes=node_attrs)

    if prune_unused:
        connections = set()
        for cg in genome.connections.values():
            if cg.enabled or show_disabled:
                connections.add((cg.in_node_id, cg.out_node_id))

        used_nodes = copy.copy(outputs)
        pending = copy.copy(outputs)
        while pending:
            new_pending = set()
            for a, b in connections:
                if b in pending and a not in used_nodes:
                    new_pending.add(a)
                    used_nodes.add(a)
            pending = new_pending
    else:
        used_nodes = set(genome.nodes.keys())

    for n in used_nodes:
        if n in inputs or n in outputs:
            continue

        attrs = {'style': 'filled',
                 'fillcolor': node_colors.get(n, 'white')}
        node_label = f'{genome.nodes[n].activation}\nb = {genome.nodes[n].bias:.2f}'
        dot.node(str(n), label=node_label, _attributes=attrs)

    for cg in genome.connections.values():
        if cg.enabled or show_disabled:
            # if cg.input not in used_nodes or cg.output not in used_nodes:
            #    continue
            input, output = cg.key
            a = node_names.get(input, str(input))
            b = node_names.get(output, str(output))
            style = 'solid' if cg.enabled else 'dashed'
            color = 'green' if cg.weight > 0 else 'red'
            width = str(0.1 + abs(cg.weight))
            dot.edge(a, b, label=f'{cg.weight:.2f}',
                     _attributes={'style': style, 'color': color, 'penwidth': width, 'fontsize': '14'})

    dot.render(path, view=view)

    return dot
