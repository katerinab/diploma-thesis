from time import time

import torch
from stable_baselines3 import DQN
from stable_baselines3.common.callbacks import StopTrainingOnRewardThreshold
from torch.utils.tensorboard import SummaryWriter

from algorithms.algorithm_interface import AlgorithmInterface
from algorithms.dqn_extensions.custom_dqn import CustomDQN
from algorithms.neat_extensions.multigenome import MultiGenome
from algorithms.hyperneat_classes.activations import str_to_activation_layer
from algorithms.hyperneat_classes.genome_handler import GenomeType
from utils.dqn_reporter import ReporterCallback, evaluate_policy
from utils.experiment_config import ExperimentConfig
from utils.plot_ut import *


class Dqn(AlgorithmInterface):
    """
    Wrapper class for DQN implementation from stable baselines 3.
    Initialized the DQN model and the logger.

    :param main_config: Experiment config
    :param code_dir: Directory where the results are saved
    :param experiment_name: Name of the experiment, serves as identifier to save the results
    :param run_id: Id of the run to distinguish between different runs with the same hyper parameters
    :param max_reward: Reward where the environment is considered to be solved
    """

    def __init__(self, main_config: ExperimentConfig, code_dir: Path, experiment_name: str, run_id: int,
                 max_reward: int):
        super().__init__(experiment_cfg=main_config, code_dir=code_dir, exp_name=experiment_name + "_DQN",
                         run_id=run_id)
        self.exp_cfg = main_config

        env = self.make_env()
        self.n_observations = env.observation_space.shape[0]

        # stats + logger
        self.max_reward = max_reward
        self.logger: ReporterCallback
        self.num_time_steps = main_config.max_train_iterations * main_config.max_episode_length * main_config.num_valid_evaluations

        # Stop training when the model reaches the reward threshold
        self.logger = ReporterCallback(eval_env=self.get_vec_env(max_episode_steps=self.exp_cfg.max_episode_length),
                                       num_val_episodes=self.exp_cfg.num_valid_evaluations,
                                       eval_freq=self.exp_cfg.num_valid_evaluations * self.exp_cfg.max_episode_length,
                                       max_episode_length=self.exp_cfg.max_episode_length,
                                       path=self.logger_path,
                                       callback_on_new_best=StopTrainingOnRewardThreshold(
                                           reward_threshold=self.max_reward, verbose=1),
                                       bsf_solution_path=self.best_so_far_dqn_solution_path
                                       )
        policy_kwargs = dict(
            activation_fn=str_to_activation_layer[self.exp_cfg.nn_hidden_activation],
            net_arch=[self.exp_cfg.num_nodes_per_hidden_layer for _ in range(self.exp_cfg.num_layers)],

        )

        self.model = CustomDQN('MlpPolicy', env=self.get_vec_env(max_episode_steps=self.exp_cfg.max_episode_length),
                               verbose=False, policy_kwargs=policy_kwargs, tensorboard_log=self.logs_path,
                               seed=self.exp_cfg.seed, device=self.exp_cfg.device,
                               batch_size=self.exp_cfg.dqn_batch_size,
                               train_freq=self.exp_cfg.dqn_num_rollouts, gamma=self.exp_cfg.gamma,
                               target_update_interval=self.exp_cfg.target_update_frequency,
                               learning_starts=0 if self.exp_cfg.load_replay_buffer else self.exp_cfg.default_learning_starts,
                               external_policy_threshold_time_steps=self.exp_cfg.external_policy_threshold_time_steps,
                               use_external_policy=self.exp_cfg.use_external_policy
                               )

    def save_configs(self):
        self.exp_cfg.save(self.save_experiment_path)

    def save_final_result(self, best_results):
        best_result = best_results[1]
        best_result.save(self.solution_path)

    def load_best_solution(self):
        model = DQN.load(self.best_so_far_dqn_solution_path)
        return model

    def load_solution(self, initial_solution, force_full_update, replay_buffer_path=None):
        """
        Load initializing solution (from HyperNEAT).

        :param initial_solution: Parameters of the neural network in form of a state dict
        :param force_full_update: Whether all keys in the state dict should be matched
        :param replay_buffer_path: Path of the prerecorded buffer
        """
        if replay_buffer_path is not None:
            self.model.load_replay_buffer(replay_buffer_path)
            self.model.num_timesteps = self.exp_cfg.prefilled_buffer_size
        if self.exp_cfg.init_q_net:
            missing_keys, unexpected_keys = self.model.policy.load_state_dict(initial_solution,
                                                                              strict=force_full_update)
            print(f"{missing_keys=} {unexpected_keys=}")
            if force_full_update:
                assert len(missing_keys) == 0, f"Some values were not set from initial solution: {missing_keys=}"
                assert len(
                    unexpected_keys) == 0, f"Some values were not set correctly from init solution: {unexpected_keys=}"

    def train(self) -> Tuple[Tuple[Optional[MultiGenome], DQN], float]:
        """
        Train the model and return the final solution.
        """
        self.model.learn(total_timesteps=self.num_time_steps - self.model.num_timesteps, callback=self.logger,
                         tb_log_name=f"run{self.run_id}", reset_num_timesteps=False)

        # Load the trained agent
        mean_reward, std_reward = evaluate_policy(self.model,
                                                  env=self.get_vec_env(
                                                      max_episode_steps=self.exp_cfg.max_episode_length),
                                                  n_eval_episodes=self.exp_cfg.max_test_iterations,
                                                  max_episode_length=self.exp_cfg.max_episode_length)

        return (None, self.model), mean_reward

    def plot_results(self, models, exp_id, duration, device):
        """
        Plot the final solution - training performance and tensorboard loss log
        :param models: the DQN model
        :param exp_id: experiment id
        :param duration: experiment duration
        :param device: device for tensors
        """
        model = models[1]
        plot_stats(self.logger, self.single_stats_path,
                   subtitle=self.experiment_name.replace("_", " ") + f"\n{duration=:.2f} s",
                   current_state_name='current')
        tb = SummaryWriter(self.tensorboard_path)
        tb.add_graph(model.policy, torch.ones(1, self.n_observations, device=device))
        for key, layer_tensor in model.policy.state_dict().items():
            tb.add_histogram(key, layer_tensor)
        tb.close()


if __name__ == '__main__':
    environment, maximal_reward = ('CartPole-v1', 195)
    code_directory = Path(__file__).resolve().parent.parent.parent
    config_path = code_directory.joinpath('configs', 'config.yml')
    SEEDS = [260679797, 701623387, 882840679, 157005113, 871591532, 609579736, 73563671, 312938638, 180649226,
             403804907]

    experiment_id = 1
    experiment_config = ExperimentConfig.create(alg_name='DQN', cfg_path=config_path, env_name=environment,
                                                seed=SEEDS[0], device='cuda', gen_type=GenomeType.W_B_1,
                                                gamma=0.99, target_update_frequency=5000,
                                                replay_buffer_epsilon=None, init_q_net=True,
                                                )
    experiment_config.external_policy_threshold_time_steps = 0
    start = time()

    print(f'Running experiment {environment=} {experiment_config.seed=}')
    exp_name = f'exp{experiment_id:03}_{experiment_config.algorithm_name}_{environment}_run{0}'
    algorithm = Dqn(experiment_config, code_directory, exp_name, 0, maximal_reward)

    (_, final_model), best_fitness = algorithm.train()
    print(f"\n\n\n--------------------\nFinal performance: {best_fitness}")
    run_duration = time() - start
    algorithm.plot_results(models=(_, final_model), exp_id=experiment_id, duration=run_duration,
                           device=experiment_config.device)
    algorithm.save((_, final_model))
    print(f'Experiment {exp_name} run for {run_duration} s which is {run_duration / 60} min {run_duration % 60} s')
