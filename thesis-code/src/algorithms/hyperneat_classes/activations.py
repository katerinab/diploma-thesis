import torch
import torch.nn.functional as f

str_to_activation_layer = {
    'relu': torch.nn.ReLU,
    'sigmoid': torch.nn.Sigmoid,
    'identity': torch.nn.Identity,
    'tanh': torch.nn.Tanh,
    'leakyrelu': torch.nn.LeakyReLU,
    'selu': torch.nn.SELU
}

str_to_activation_function = {
    'clip': lambda x: x.clamp(min=-200, max=200),
    'relu': torch.relu,
    'selu': torch.selu,
    'leakyrelu': f.leaky_relu,
    'sigmoid': torch.sigmoid,
    'tanh': torch.tanh,
    'sin': torch.sin,
    'log': torch.log,
    'abs': torch.abs,
    'square': torch.square,
    'identity': lambda x: x,
    'cube': lambda x: x ** 3,
    'gauss': lambda x: torch.exp(-5.0 * (x ** 2)),
}
