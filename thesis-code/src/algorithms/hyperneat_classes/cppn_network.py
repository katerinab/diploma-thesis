import neat
import networkx as nx
import numpy as np
import torch
import torch.nn as nn

from algorithms.hyperneat_classes import activations


class CppnNetwork(nn.Module):
    """
    PyTorch-based neural network that represents HyperNEAT CPPN.
    The network is created from Genome, that is translated to directed acyclic graph.
    The network is not structured in layers, each tensor represents on neuron in the neural network.
    :param genome: HyperNEAT genome (with DAG structure) to be translated to PyTorch,
    :param config: NEAT config file,
    :param graph: DAG representing the CPPN,
    :param device: Device for the tensors,
    :param dtype: Data type of the tensors,
    """

    def __init__(
            self,
            genome: neat.DefaultGenome,
            config: neat.genome.DefaultGenomeConfig,
            graph: nx.DiGraph,
            device: str,
            dtype=torch.float,
    ):
        super(CppnNetwork, self).__init__()
        self.genome = genome
        self.config = config
        self.G = graph
        self.dtype = dtype
        self.device = device

        self.nn_nodes = {}

        self.node_to_str_bias = {n: f"b_{n}" for n in filter(lambda n: n not in config.input_keys, self.G.nodes)}
        self.edge_to_str_w = {(u, v): f"w_{u}_{v}" for (u, v) in self.G.edges}
        self.str_to_node_bias = {value: key for key, value in self.node_to_str_bias.items()}
        self.str_to_edge_w = {value: key for key, value in self.edge_to_str_w.items()}

        self.weights_e_key_to_tensor = nn.ParameterDict({
            self.edge_to_str_w[(from_node, to_node)]:
                nn.Parameter(torch.tensor(
                    [self.G.edges[from_node, to_node]["weight"]],
                    dtype=dtype, device=device, requires_grad=True))
            for (from_node, to_node) in self.G.edges
        })

        self.biases_n_key_to_tensor = nn.ParameterDict({
            self.node_to_str_bias[node_id]: nn.Parameter(
                torch.tensor([self.G.nodes[node_id]["bias"]], dtype=dtype, device=device, requires_grad=True))
            for node_id in filter(lambda item: item not in config.input_keys, self.G.nodes)
        })

    def predict(self, x: torch.tensor):
        with torch.no_grad():
            weights = self.forward(x)
        return weights

    def forward(self, x: torch.tensor) -> torch.tensor:
        for input_id in range(len(self.config.input_keys)):
            node_id = self.config.input_keys[input_id]
            self.nn_nodes[node_id] = x[:, input_id]

        for node_id in nx.topological_sort(self.G):
            if node_id in self.config.input_keys:
                continue
            parents = [self.weights_e_key_to_tensor[self.edge_to_str_w[(pred, node_id)]] * self.nn_nodes[pred]
                       for pred in self.G.predecessors(node_id)]
            activation = activations.str_to_activation_function[self.G.nodes[node_id]['act']]
            if len(parents) == 0:
                self.nn_nodes[node_id] = activation(self.biases_n_key_to_tensor[self.node_to_str_bias[node_id]]) \
                    .expand(self.nn_nodes[-1].shape)
            else:
                self.nn_nodes[node_id] = activation(torch.stack(parents).sum(dim=0)
                                                    + self.biases_n_key_to_tensor[self.node_to_str_bias[node_id]]
                                                    )
        weights = torch.stack([self.nn_nodes[node_id] for node_id in self.config.output_keys], dim=1)
        if weights.shape[1] == 2:
            mask = np.ones(weights.shape[0])
            mask[weights.cpu()[:, 1] < 0.5] = 0
            mask_tensor = torch.tensor(mask, dtype=self.dtype, device=self.device)
            weights = (weights[:, 0] * mask_tensor).view((weights.shape[0], 1))
        return weights

    @staticmethod
    def create(
            genome: neat.DefaultGenome,
            config: neat.genome.DefaultGenomeConfig,
            device: str
    ):
        """

        Method to create the CPPN network.

        :param genome: HyperNEAT genome (with DAG structure) to be translated to PyTorch,
        :param config: NEAT config file,
        :param device: Device for the tensors,
        :return: The created PyTorch module
        """
        graph = nx.DiGraph()
        node_list = [(node_id, {"bias": node.bias, "act": node.activation}) for node_id, node in genome.nodes.items()]
        edges_list = [(from_node, to_node, edge.weight) for (from_node, to_node), edge in
                      filter(lambda kv_pair: kv_pair[1].enabled, genome.connections.items())]
        input_nodes_list = [(node_id * -1, {"bias": 0, "act": activations.str_to_activation_function["identity"]})
                            for node_id in range(1, len(config.input_keys) + 1)]
        graph.add_nodes_from(node_list)
        graph.add_nodes_from(input_nodes_list)
        graph.add_weighted_edges_from(edges_list)
        assert graph.number_of_nodes() == len(
            genome.nodes) + len(
            config.input_keys), "number of parameter nodes should be the same as the number of nodes" \
                                " in the created graph"
        assert nx.algorithms.dag.is_directed_acyclic_graph(graph), "the network should be directed acyclic graph"
        assert all(graph.has_node(n) for n in config.input_keys), "all input keys must be present in the created graph"
        assert all(
            graph.has_node(n) for n in config.output_keys), "all output keys must be present in the created graph"
        isolated_hidden_nodes = set(nx.isolates(graph)) - set(config.input_keys) - set(config.output_keys)
        graph.remove_nodes_from(list(isolated_hidden_nodes))

        return CppnNetwork(
            genome=genome,
            config=config,
            graph=graph,
            device=device,
        )
