from dataclasses import dataclass
from pathlib import Path
from typing import Optional

from simple_parsing.helpers import Serializable
from simple_parsing.helpers.serialization import encode, register_decoding_fn

from algorithms.hyperneat_classes.genome_handler import GenomeType


@dataclass
class ExperimentConfig(Serializable):
    algorithm_name: Optional[str] = None
    exp_id: Optional[int] = None
    run_id: Optional[int] = None
    seed: Optional[int] = None
    environment: Optional[str] = None
    max_train_iterations: int = None
    num_valid_evaluations: int = None
    max_test_iterations: int = None
    max_episode_length: int = None
    device: str = None
    num_layers: int = None
    num_nodes_per_hidden_layer: int = None
    nn_out_activation: str = None
    nn_hidden_activation: str = None
    n_processes: int = 1
    genome_type: Optional[GenomeType] = None
    pop_size: Optional[int] = None
    dqn_num_rollouts: int = None
    dqn_batch_size: int = None
    gamma: float = None
    target_update_frequency: int = None
    initialize_both_q_nets: bool = None
    load_replay_buffer: bool = None
    prefilled_buffer_size: int = None
    default_learning_starts: int = None
    replay_buffer_epsilon: float = None
    init_q_net: bool = None
    external_policy_threshold_time_steps: int = None
    use_external_policy: bool = None
    cppn_backprop_quality_threshold: float = None
    cppn_num_backprop_epochs: int = None
    num_iterations_in_hn_dqn_loop: int = None

    @classmethod
    def create(cls, alg_name: str, cfg_path: Path, env_name: str, seed: int, device: str,
               gen_type: Optional[GenomeType], gamma: Optional[float], target_update_frequency: Optional[int],
               replay_buffer_epsilon: Optional[float], init_q_net):
        cfg = ExperimentConfig.load(cfg_path)
        cfg.genome_type = gen_type
        cfg.environment = env_name
        cfg.seed = seed
        cfg.device = device
        cfg.algorithm_name = alg_name
        cfg.gamma = gamma
        cfg.replay_buffer_epsilon = replay_buffer_epsilon
        cfg.target_update_frequency = target_update_frequency
        cfg.init_q_net = init_q_net
        assert alg_name in ["HyperNEAT", "DQN", "Joined_HyperNEAT_DQN",
                            "HyperNEAT_DQN_loop"], f"Invalid algorithm name was set: {alg_name}"
        assert alg_name not in ["HyperNEAT", "Joined_HyperNEAT_DQN",
                                "HyperNEAT_DQN_loop"] or cfg.genome_type is not None, "Must set genome for HyperNEAT"
        assert alg_name not in ["DQN", "Joined_HyperNEAT_DQN", "HyperNEAT_DQN_loop"] or (
                    cfg.dqn_batch_size is not None and cfg.dqn_num_rollouts is not None), "Must set batch size and rollouts for DQN"
        assert alg_name not in ["Joined_HyperNEAT_DQN", "HyperNEAT_DQN_loop"] or (
                    cfg.replay_buffer_epsilon is not None and cfg.init_q_net is not None), "Must set replay_buffer_epsilon for Joined_HyperNEAT_DQN"
        assert alg_name not in ["HyperNEAT_DQN_loop"] or (
                    cfg.cppn_backprop_quality_threshold is not None and cfg.cppn_num_backprop_epochs is not None and cfg.num_iterations_in_hn_dqn_loop is not None), "Must set all data for HyperNEAT_DQN_loop"

        return cfg


@encode.register
def encode_genome_type(obj: GenomeType) -> str:
    return obj.value


register_decoding_fn(GenomeType, lambda value: GenomeType(value))

if __name__ == '__main__':
    path = Path(__file__).resolve().parent.parent.parent.joinpath('configs', 'config.yml')
    config = ExperimentConfig.create('DQN', path, 'name', 12345, 'cpu', GenomeType.W_B_1, gamma=0.99,
                                     target_update_frequency=10000, replay_buffer_epsilon=0.1, init_q_net=True)
    print(config)

    save_path = Path(__file__).resolve().parent.parent.parent.joinpath('configs', 'config_saved.yml')
    config.save(save_path)

    loaded_cfg = ExperimentConfig.load(save_path)
    print(loaded_cfg)
