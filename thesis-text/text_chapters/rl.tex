\section{Reinforcement learning} \label{sec:rl}

Reinforcement learning aims to design an agent that maps states to actions to maximise the agent's reward in the environment. The information from this chapter was obtained in~\cite{pechoucek_lecture_2021}~and~\cite{russell_artificial_2009}.

\subsection{Background}

Reinforcement learning model is defined as
\begin{itemize}
    \item $S$ -- set of states, can be discrete or continuous 
    \item $A$ -- set of actions the agent can perform, can be discrete or continuous 
    \item $R: S \times A \to \mathbb{R}$ -- reward function determining reward $r \in \mathbb{R}$ that the agent gets after performing action $a \in A$ in state $s \in S$ 
    \item $t$ -- discrete time steps, $t \in [0, \dots, T]$
    \item $P: S \times A \times S \to [0, 1]$ -- probability function $P(s'| s, a)$ defining the probability that agent applying action $a$ in state $s$ gets to state $s'$
\end{itemize}

The model of the agent and the environment is shown in Figure \ref{fig:rl_markov}. The agent has a policy $\pi: S \to A$ that, given a state $s$, outputs action $a$ that the agent should take. The goal of the reinforcement learning methods is to design $\pi^*$ that maximises the reward of the agent in the environment.

\begin{figure}[hbt]
    \centering
    \includegraphics[width=0.55\textwidth]{fig/rl.pdf}
    \caption[Reinforcement learning: Markov decission process diagram]{Reinforcement learning as Markov decission process}
    \label{fig:rl_markov}
\end{figure}

A value function $V_\pi: S \to \mathbb{R}$ assigns each state $s$ the expected mean reward obtained by following policy $\pi$ from state $s$. A action-value function $Q_\pi: S \times A \to \mathbb{R}$ assigns each pair $s, a$ the expected mean reward obtained by following policy $\pi$ from state $s$.

The $V$- and $Q$-values are tied by the Bellman optimality equation. The equation states for $Q^*$ that
\begin{equation} \label{eq:bellman}
    Q^*(s,a) = \displaystyle\sum_{s', r} P(s' | s, a) [r + \gamma \max_{a'}Q^*(s', a')]
\end{equation}
where $\gamma \in (0, 1]$ is a discount factor.

To find the optimal policy, we would need to evaluate all possible states and actions in iterations to find a converging solution. This would require knowledge of reward function $R$ and probability distribution $P$. Apart from that, the action and state-space would need to be finite and reasonably small as keeping the $Q$-value table would require $|S| \times |A|$ entries. If these conditions were met, we could find the optimal solution using Value- and Policy-iteration algorithms that iteratively update the $Q$-table and policy to obtain the optimal solution in the Bellman sense.

There are different algorithms used for unknown MDPs that learn $Q$-values by sampling the search space.

These methods are based on the temporal difference, which stands for a difference in estimated $Q$-values in consequent time steps. The methods are either on-policy, meaning that the current policy is used to select an action for the next state $s'$, or off-policy, meaning that action maximising the value of the next state is selected, ignoring the current policy.

Q-learning is an example of an off-policy algorithm with the one-step update
\begin{equation}
    Q(s, a) \gets Q(s, a) + \alpha [r + \gamma max_{a'}Q(s', a') - Q(s, a)]
\end{equation}

SARSA is an example of an on-policy algorithm with the one-step update
\begin{equation}
    Q(s, a) \gets Q(s, a) + \alpha [r + \gamma Q(s', a') - Q(s, a)]
\end{equation}
where $a'$ is an action chosen by policy $\pi$.


To learn the $Q$-values properly, it is necessary to follow an exploration policy that satisfies GLIE properties \cite{singh_convergence_2000}. GLIE stands for greedy in the limit (in the limit, the policy should choose the maximising action) and infinite exploration (state-action pairs are visited an infinite number of times). In practice, this is ensured by using $\epsilon$-greedy policy that chooses the maximising action with probability $1-\epsilon$ and random action with probability $\epsilon$ where~$\epsilon$~decreases with the number of iterations.

In this thesis, we work with a deterministic environment with continuous state space, discrete action space and we use a method based on $Q$-value estimation. The presented approach should work for stochastic environments and discrete state space as well, whereas using continuous action space would require a modification of the local search part of the algorithm.

\subsection{Deep Q-learning Networks} \label{sec:dqn}

Deep Q-learning Networks is an algorithm \cite{mnih_playing_2013} designed to be able to leverage Q-learning capabilities in huge state space. To do so, the authors approximate the $Q$-table by a $Q$-function that is represented by a neural network.

The algorithm (see Algorithm \ref{alg:dqn}) utilises experience replay where one experience is a tuple $(s_t, a_t, r_t, s_{t+1})$. Each experience is stored in a replay buffer $D$, which serves as a dataset to be fed to the neural network by random sampling in mini-batches. The real value $y$ is set to the $Q$-value estimated by the neural network. The target value $y'$ corresponds to the one-step lookahead taking into account the next state $Q$-value and the obtained reward. The gradient step is performed based on the mean square error of $y$ and $y'$. To make the algorithm more stable, two different networks $Q$-network $Q$ and $Q$-target-network $\hat{Q}$ are used. To simplify the notation, in the sequel, we use $Q$-target for $Q$-network-target. $Q$ represents the current solution, is used to generate $y$ and is being updated by the gradient step. $\hat{Q}$ is an older version of $Q$ that is used to generate $y'$. $\hat{Q}$ is updated every $C$ steps by the current version of $Q$.

\begin{algorithm}[th]
    \caption[DQN pseudocode]{DQN pseudocode adapted from \cite{mnih_human-level_2015}}\label{alg:dqn}
    \begin{algorithmic}[1]
    \State Initialize replay buffer $D$ to capacity $N$
    \State Initialize action-value function $Q$ with random weights $\theta$
    \State Initialize target action-value function $\hat{Q}$ with random weights $\theta^- = \theta$
    \For{episode $= 1, M$}
        \State Initialize sequence $s_1$
        \For{$t = 1, T$}
            \State With probability $\epsilon$ select a random action $a_t$
            \State otherwise select $a_t = \argmax_a Q(s_t, a; \theta)$
            \State Execute action $a_t$ in emulator and observe reward $r_t$ and state $s_{t+1}$
            \State Store transition $(s_t, a_t, r_t, s_{t+1})$ in $D$
            \State Sample random minibatch of transitions $(s_j, a_j, r_j, s_{j+1})$ from $D$
            \State Set $y_j = r_j$ if episode terminates at step $j+1$
            \State Set $y_j = r_j + \gamma \max_{a'} \hat{Q}(s_{j+1}, a'; \theta^-)$ otherwise
            \State Perform a gradient descent step on $(y_j - Q(s_j, a_j; \theta))^2$ with respect to $\theta$
            \State Every $C$ steps reset $\hat{Q} = Q$
        \EndFor
    \EndFor
    \end{algorithmic}
\end{algorithm}

The method leverages the following:
\begin{itemize}
    \item Data efficiency: The classical Q-learning generates a data sample from the environment for each Q-value update (as 1:1 mapping of sample to update). On the contrary, DQN uses a replay buffer $D$ that is gradually being filled by data samples. When performing an update, a random batch of data is sampled from the replay buffer. This allows to use each data sample more times (as 1:N mapping of sample to update) and is especially beneficial for cases when the environment evaluation is costly.
    \item Sample randomisation: Learning from consecutive samples is inefficient because the samples are correlated. By sampling from the replay buffer $D$, we can get data from several different episodes, making the training faster as the sample distribution is more representative.
    \item Training stability: DQN uses $Q$ as a policy, and $\hat{Q}$ as a target policy. $\hat{Q}$ is a fixed target that is updated every $C$ time steps by the current parameters from $Q$. In the meantime, only $Q$ is being modified by the calculated gradients, which improves convergence as $\hat{Q}$ is a fixed, stable target. If $Q$ was used for target calculation instead of $\hat{Q}$, the target value would change for each update, and the algorithm could start oscillating.
\end{itemize}
 
The authors have tested the algorithm on various Atari 2600 games and achieved a human-level performance. The agent was receiving high-dimensional preprocessed frame pixels and game scores on the input. Moreover, the hyperparameters and architecture were fixed for all the different game types. A disadvantage of DQN is that it produces a finite-length vector where each feature in the vector corresponds to one action in the action space. This limits the action space to be a discrete finite set.

\subsection{Testing platforms}

The algorithm is to be evaluated in the OpenAI gym environments or its alternatives. OpenAI gym \cite{brockman_openai_2016} is an open-source catalogue of RL environments. The advantage of this library is that it has a very clean API, is well-tested and used by many researchers, which is convenient for performance benchmarking. There are five main categories of environments in OpenAI gym (some of them are dependent on the MuJoCo simulator described below):
\begin{itemize}
    \item Atari -- 59 different Atari environments for Atari 2600 video game console
    \item Box2D -- continuous control tasks in the Box2d simulator
    \item Classic control -- control theory problems from the classic RL literature like the cart pole or inverted pendulum
    \item MuJoCo -- continuous control tasks such as humanoid or 4-legged robot
    \item Robotics -- goal-based tasks for the Fetch and ShadowHand robots in MuJoCo simulator
\end{itemize}

MuJoCo \cite{todorov_mujoco_2012} is a physics engine for fast and accurate simulation, mainly used in robotics and biomechanics domain. The simulator allows defining custom environments. The humanoid environment (also defined in OpenAI gym) is often used to test RL tasks; see, for example, this video \url{https://youtu.be/iJlEbHsgM7Q}. However, the usability of the simulator is limited as it is licensed, with a 30-day free trial or a free license for students.

Robogym \cite{openai_robogym_2020} is an open-source library that provides a wrapper Python API that is the same as in OpenAI gym to run several different environments in MuJoCo. There are two types of environments. Firstly, Dactyl environments where the goal is a robotic hand that has 20 actuated degrees of freedom to manipulate a Rubik's cube. Different complexity levels are utilised by restricting the degrees of freedom of the Rubik's cube. Secondly, Rearrange environments where the goal is to use a robotic arm with a gripper to set up items on a table in a requested way.

An interesting alternative to OpenAI gym that heavily depends on the licensed MuJoCo simulator is PyBullet Gymperium \cite{ellenberger_pybullet_2019}. It is an open-source reimplementation of the OpenAI Gym MuJoCo environments, and Roboschool environments \cite{openai_roboschool_2017} based on the Bullet Physics simulator. An advantage of this package is that the environments are defined using the OpenAI gym API. There are also plenty of other RL environments one could use for experiments. I found the list at \url{https://github.com/clvrai/awesome-rl-envs} to be the most comprehensive.

In this thesis, we use the classic control environments with discrete action space from OpenAI gym package for experiments to keep the thesis open-source while considering the fact that the usage of the DQN algorithm implies the action space to be discrete.


