import time
import warnings
from pathlib import Path
from typing import Optional, Union, Dict, Any, Tuple

import gym
import numpy as np
from stable_baselines3.common import base_class
from stable_baselines3.common.callbacks import EventCallback, BaseCallback
from stable_baselines3.common.vec_env import VecEnv, sync_envs_normalization, DummyVecEnv

# based on EvalCall from stable baselines
from utils.stats import Stats, GenerationStats


def evaluate_policy(
        model: "base_class.BaseAlgorithm",
        env: Union[gym.Env, VecEnv],
        n_eval_episodes: int,
        max_episode_length: int,
) -> Tuple[float, float]:
    """
    Adapted from DQN stable baselines 3

    Runs policy for ``n_eval_episodes`` episodes and returns average reward.
    This is made to work only with one env.

    .. note::
        If environment has not been wrapped with ``Monitor`` wrapper, reward and
        episode lengths are counted as it appears with ``env.step`` calls. If
        the environment contains wrappers that modify rewards or episode lengths
        (e.g. reward scaling, early episode reset), these will affect the evaluation
        results as well. You can avoid this by wrapping environment with ``Monitor``
        wrapper before anything else.

    :param model: The RL agent you want to evaluate.
    :param env: The gym environment. In the case of a ``VecEnv``
        this must contain only one environment.
    :param n_eval_episodes: Number of episode to evaluate the agent
    :param max_episode_length: Maximum number of time steps in one episodes after which the environment will be interrupted
    and reseted.
    :return: Mean reward per episode, std of reward per episode.
        Returns ([float], [int]) when ``return_episode_rewards`` is True, first
        list containing per-episode rewards and second containing per-episode lengths
        (in number of steps).
    """

    if isinstance(env, VecEnv):
        assert env.num_envs == 1, "You must pass only one environment when using this function"

    episode_rewards, episode_lengths = [], []
    while len(episode_rewards) < n_eval_episodes:
        # Number of loops here might differ from true episodes
        # played, if underlying wrappers modify episode lengths.
        # Avoid double reset, as VecEnv are reset automatically.
        obs = env.reset()
        done, state = False, None
        episode_reward = 0
        episode_length = 0
        while not done and episode_length < max_episode_length:
            action, state = model.predict(obs, deterministic=True)
            obs, reward, done, info = env.step(action)
            episode_reward += reward.item()
            episode_length += 1
        else:
            episode_rewards.append(episode_reward)
            episode_lengths.append(episode_length)

    mean_reward = np.mean(episode_rewards)
    std_reward = np.std(episode_rewards)

    return mean_reward.item(), std_reward.item()


class ReporterCallback(EventCallback):
    """
    Callback for evaluating an agent.

    :param eval_env: The environment used for initialization
    :param callback_on_new_best: Callback to trigger
        when there is a new best model according to the ``mean_reward``
    :param eval_freq: Evaluate the agent every eval_freq call of the callback.
        will be saved. It will be updated at each evaluation.
        according to performance on the eval env will be saved.
        use a stochastic or deterministic actions.
    :param verbose:
    """

    def __init__(
            self,
            eval_env: Union[gym.Env, VecEnv],
            num_val_episodes: int,
            max_episode_length: int,
            eval_freq: int,
            path: Path,
            bsf_solution_path: Path,
            callback_on_new_best: Optional[BaseCallback] = None,
            verbose: int = 1,
    ):
        super(ReporterCallback, self).__init__(callback_on_new_best, verbose=verbose)
        self.num_val_episodes = num_val_episodes
        self.eval_freq = eval_freq
        self.best_mean_reward = -np.inf
        self.eval_env = eval_env
        self.max_episode_length = max_episode_length

        # Convert to VecEnv for consistency
        if not isinstance(eval_env, VecEnv):
            eval_env = DummyVecEnv([lambda: eval_env])
        if isinstance(eval_env, VecEnv):
            assert eval_env.num_envs == 1, "You must pass only one environment for evaluation"

        self.log_file = path
        self.bsf_solution_path = bsf_solution_path
        self.generation: Optional[int] = None
        self.generation_start_time: Optional[float] = None
        self.stats: Stats = Stats(generation_stats=[])

    def _init_callback(self) -> None:
        print(f"Calling init callback with {self.n_calls=}")
        # Does not work in some corner cases, where the wrapper is not the same
        if not isinstance(self.training_env, type(self.eval_env)):
            warnings.warn("Training and eval env are not of the same type" f"{self.training_env} != {self.eval_env}")
        self.start_generation(generation=0)

    def start_generation(self, generation):
        self.generation = generation
        self.stats.generation_stats += [GenerationStats(species={})]
        self.stats.generation_stats[self.generation].generation = generation
        self.generation_start_time = time.time()

    def _on_training_end(self) -> None:
        sync_envs_normalization(self.training_env, self.eval_env)
        if len(self.stats.generation_stats) < 2:
            return
        if self.stats.generation_stats[-1].generation == self.stats.generation_stats[-2].generation + 1:
            self.stats.generation_stats = self.stats.generation_stats[:-1]
            self.generation -= 1
            self.generation_start_time = None
        else:
            # save the rest of stats if evaluation frequency is larger than 1
            self.process_stats()

    def process_stats(self) -> int:
        mean_reward, std_reward = evaluate_policy(
            self.model,
            self.eval_env,
            n_eval_episodes=self.num_val_episodes,
            max_episode_length=self.max_episode_length
        )

        trigger_callback = False
        if mean_reward > self.best_mean_reward:
            self.best_mean_reward = mean_reward
            self.model.save(self.bsf_solution_path)
            trigger_callback = self.callback is not None

        self.stats.generation_stats[self.generation].time_steps = self.num_timesteps
        self.stats.generation_stats[self.generation].fitness_avg = mean_reward
        self.stats.generation_stats[self.generation].fitness_std = std_reward
        self.stats.generation_stats[self.generation].fitness_best = self.best_mean_reward
        self.stats.generation_stats[self.generation].time_elapsed = time.time() - self.generation_start_time

        print(f'gen={self.generation} best(all)={self.stats.generation_stats[self.generation].fitness_best} '
              f'avg(gen)={self.stats.generation_stats[self.generation].fitness_avg} '
              f'std(gen)={self.stats.generation_stats[self.generation].fitness_std} where gen saved every {self.eval_freq} time steps')

        self.stats.save(self.log_file)
        return trigger_callback

    def _on_step(self) -> bool:
        if self.eval_freq > 0 and self.n_calls % self.eval_freq == 0:
            # Sync training and eval env if there is VecNormalize
            sync_envs_normalization(self.training_env, self.eval_env)
            trigger_callback = self.process_stats()
            self.start_generation(self.generation + 1)
            if trigger_callback:
                return self._on_event()
        return True

    def update_child_locals(self, locals_: Dict[str, Any]) -> None:
        """
        Update the references to the local variables.

        :param locals_: the local variables during rollout collection
        """
        if self.callback:
            self.callback.update_locals(locals_)
