from dataclasses import dataclass
from typing import Dict, List, Optional

from simple_parsing import Serializable


@dataclass
class GenerationStats(Serializable):
    species: Dict[int, int]
    generation: Optional[int] = None
    time_steps: Optional[int] = None
    pop_size: Optional[int] = None
    n_species: Optional[int] = None
    time_elapsed: Optional[float] = None

    n_extinctions: Optional[int] = None
    fitness_avg: Optional[float] = None
    fitness_std: Optional[float] = None
    fitness_best: Optional[float] = None
    n_neurons_best: Optional[int] = None
    n_conns_best: Optional[int] = None


@dataclass
class Stats(Serializable):
    generation_stats: List[GenerationStats]

    def get_mean(self):
        return [gen_stats.fitness_avg for gen_stats in self.generation_stats]

    def get_std(self):
        return [gen_stats.fitness_std for gen_stats in self.generation_stats]
