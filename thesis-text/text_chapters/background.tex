\section{Neuroevolution} \label{sec:background}

This chapter gives a background on neuroevolution. Neuroevolution is a class of methods that evolve neural networks by evolutionary algorithms. First, we describe the basics of neural networks and evolutionary algorithms. Then, we continue with the neuroevolution, and its different variants. We focus on two neuroevolutionary methods, NEAT and HyperNEAT, which are the foundation of this thesis.

\subsection{Neural networks}

This chapter gives a brief overview of neural networks that have been a widely used AI tool over the past decades~\cite{krizhevsky_imagenet_2017},~\cite{collobert_natural_2011}~and~\cite{lecun_deep_2015}. The information from this chapter was obtained in~\cite{flach_lecture_2021}~and~\cite{legenstein_lecture_2021}. Neural networks are inspired biologically by neurons in the human brain. Each neuron gets input signals from its neighbouring neurons connected by synapses, processes them and sends a signal to its neighbours. The neurons are interconnected into huge networks and together are able to represent a complex behaviour. The human brain has approximately 86 million neurons~\cite{azevedo_equal_2009}.

The artificial neurons are traditionally formed into layered architectures where each layer contains several neurons that are connected with the neurons in the preceding and successive layers. In feed-forward architectures, the neuron gets input from its preceding neurons weighted by the connecting edges, sums the weighted inputs together, adds a neuron-specific threshold (bias) and performs a non-linear activation function (see Figure \ref{fig:neuron}). In this way, the signal gradually travels from the first input layer to the last output layer (see Figure \ref{fig:architecture}).

\begin{figure}[hbt]
    \centering
    \includegraphics[width=0.6\textwidth]{fig/neuron_model.pdf}
    \caption[Neural networks: Artificial neuron]{Artificial neuron~\cite{mcculloch_logical_1943} with input $\mathbf{x}=(x_1, x_2, \dots, x_n)$ and output $\varphi(b_j + \sum_i w_{i,j} \cdot x_i)$.}
    \label{fig:neuron}
\end{figure}

The goal is to correctly design the architecture with the parameters being mainly the number and types of layers and the way they are interconnected, the number of neurons in each layer, and the type of activation functions. The learning objective is to find the (sub)optimal parameters (connection weights and neuron biases) such that the performance of the network is high and generalises well to previously unseen data.

\begin{figure}[hbt]
    \centering
    \includegraphics[width=0.65\textwidth]{fig/nn_architecture.pdf}
    \caption[Neural networks: Feed-forward fully connected neural network architecture]{Feed-forward fully connected neural network architecture with four hidden layers.}
    \label{fig:architecture}
\end{figure}


A neural network is an approximator of a function that has a feature vector on the input. Such function is either used for classification or regression. In the former, the output vector of the network usually represents probabilities with which the feature vector belongs to a given class. An example of such task is a neural network that gets an image of a single written digit on the input and outputs a probability vector of size 10, saying for each possible digit, what is the probability that it was on the input. In the latter, the output represents a vector of problem-specific values. An example of such task could have a feature vector describing a movie on the input (with features such as the main actors, release year or genre) and a vector describing the percentual success of the movie (values such as revenue or user rating) on the output.

We measure the performance of the trained neural network using a loss function that compares the expected and the approximated outputs and shows how 'different' the outputs are. There are many different loss functions that are suitable for different types of problems, such as absolute error, mean square error or cross entropy. Absolute error calculates the sum of the absolute differences in the output and is suitable for regression problems where having a sparse network is beneficial as it pushes the parameters to zero. Mean square error calculates the sum of the squared differences of the outputs and is suitable for regression problems where being approximately correct in all cases is better than being correct in some cases but very incorrect in a few cases. Cross entropy is used for classification problems and measures the difference in distributions of the real and approximated classes.

When we consider the neural network in terms of linear algebra, the neurons accept a linear combination of the weighted preceding signals, and the activation functions are differentiable (or have predefined behaviour in their non-differentiable parts). Hence, we can optimise the network parameters (weights and biases) by gradient backpropagation that aims to modify the parameters so that the loss is minimised. To do that, we create a computational graph representing the compound derivatives of each parameter in the network, calculate the loss over the input samples, and modify each parameter by the calculated gradient.

There are three main practical issues connected to that. Firstly, the search can get stuck in the local optimum. Secondly, due to high memory and time requirements, we cannot possibly input all possible samples to the network and make the gradient update in one step. Lastly, we probably do not have all the possible input samples that exist, or we might not have the perfect architecture design. Ignoring these issues could result in technical infeasibility to perform the optimisation or optimising different criteria than is required. To partially resolve that, stochastic gradient descent is used (see Algorithm \ref{alg:sgd}). This method iteratively samples a small part (batch) from the dataset and performs an update over this small sample only. The relative size of the updates is controlled by a learning rate that says how much we want to change the parameters in the given iteration. It has been proved~\cite{robbins_stochastic_1951} that the learning converges if certain conditions such as properly selected dataset, learning parameters, and loss function are met. The optimisation is sensitive to getting stuck in the local optima. To mitigate this, more complex optimisation methods than SGD such as momentum~\cite{sutskever_importance_2013} or ADAM~\cite{kingma_adam_2014} are used. These methods are taking into account not only the current gradients but the combination of the previous ones as well and adaptively change the learning rate.


\begin{algorithm}[th]
    \caption[SGD pseudocode]{Stochastic gradient descent pseudocode for dataset $D$ with samples $x^i$, targets $y^i$, the number of episodes $E$, learning rate $\alpha$ and a function $h$ parametrised by $\mathbf{\theta}$}\label{alg:sgd}
    \begin{algorithmic}[1]
    \State $\mathbf{\theta} \gets $ random init
    \For{$epoch \in [1, \dots, E]$}
        \State shuffle $D$
        \For{minibatch of size $M$ containing $\mathbf{x^i}, y^i \in D$}
            \State get prediction $\hat{y}^i \gets h(\mathbf{x^i})$
            \State get loss $L \gets \frac{1}{M} \displaystyle\sum_{i = 1, \dots M} loss(\hat{y}^i, y^i)$
            \State get gradients $\Delta \mathbf{\theta} \gets -\nabla_L \mathbf{\theta}$
            \State make gradient step $\mathbf{\theta} \gets \mathbf{\theta} + \alpha \cdot \Delta \mathbf{\theta}$
        \EndFor
    \EndFor
    \end{algorithmic}
\end{algorithm}

Many problems can arise while training the network where most of them are connected to deep learning~\cite{schmidhuber_deep_2015},~\cite{lecun_deep_2015}. One of them being overfitting to the given dataset, which results in poor performance on unseen data. We can use a separate validation dataset to stop the training when the loss would still decrease on the training data, but it would increase on the validation data. Moreover, we can use dropout layers~\cite{srivastava_dropout_2014} that randomly turn off some of the connections, making the network more stable. Another problems are related to the size of the gradient. With the growing size of the network, it can happen that the gradient vanishes~\cite{hochreiter_vanishing_1998}, which results in updating only the parameters towards the end of the network. For example, this can easily happen with ReLU activation functions~\cite{nair_rectified_2010} that have zero gradient for negative input, so they are not propagating gradient any further. We can carefully design the architecture or introduce skip connections~\cite{he_deep_2015} that interconnect nodes in non-neighbouring layers to mitigate this. On the other hand, too large gradients cause a gradient explosion~\cite{pascanu_difficulty_2013} resulting in the parameters being updated from one extreme to another and not learning anything useful. Techniques such as regularisation or data normalisation~\cite{schmidhuber_deep_2015} help to avoid this. The first one pushes the parameters to stay in a reasonable range of values. The second one helps the network so that it does not have to learn how to normalise the data itself.

In the scope of the thesis, we use neural networks as simple function approximators and avoid using advanced techniques. We work with shallow networks with a moderate number of neurons only (typically 4 hidden layers, 32 neurons each). The neural networks we use are feed-forward and fully connected, meaning that there are no cycles in the computational graph, and each neuron in layer $i$ is connected to each neuron in layer $i+1$. We solve a regression task using SGD optimisation in a reinforcement learning domain. We focus mainly on combining the two algorithms (HyperNEAT and DQN) rather than finding the perfect hyperparameters for the neural networks themselves.

\subsection{Evolutionary algorithms}

Evolutionary algorithms are based on Darwin's principle of natural selection. The principle says that in a population of varied individuals, the strongest ones are preferred for reproduction. This helps the whole population to conserve the most substantial features and survive. The information from this chapter was obtained in~\cite{posik_lecture_2021} and~\cite{luke_essentials_2009}. Evolutionary algorithms mimic this behaviour by evolving a population of individuals. Each individual in the population represents a possible solution in a predefined domain (e.g. one individual can be one neural network with fixed parameters). The individual is represented by a \textit{genotype} which is encoded information defining the individual, and \textit{phenotype} which is an interpretation of the genotype. For example, if our individual was a real value, the phenotype would be the specific value, and the genotype could be its binary encoding. The individuals are evaluated by a \textit{fitness} function that is to be maximised. Fitness says how good the individual is in the given environment.

Each individual can be modified by a \textit{mutation} operator. The operator slightly changes the genotype so that part of the genotype is kept while the other part is randomly modified. Mutation pushes towards diversity in the population and helps to explore the search space by performing a local search around the current genotype. The advantage against random initialisation of a new genotype is that some of the beneficial genes are kept in the individual. An example of mutation is a bit-flip for binary representation or adding Gaussian noise for real representation.

The individuals reproduce using a \textit{crossover} operator where the typical arity of the operator is two. The idea behind the operator is that by mixing two well-performing solutions, we can get a new solution (called child or offspring) that takes the best out of its parents. One-point crossover finds a crossing point in both of the parents and combines the first part of one of the parents with the second part of the other parent to create a new offspring. Similar is a two-point crossover with the difference that there are two crossing points, and we create the offspring by combining three parts selected from the parents. Uniform crossover selects each gene of the offspring randomly from the parents.

The population of individuals evolve in generations. During each generation, a new population is created from the current population by selecting parents and applying crossover and mutation operators to create new individuals (see Algorithm \ref{alg:ea}). The new population is then evaluated by the fitness function and replaces the old population.

\begin{algorithm}[th]
    \caption[Evolutionary algorithm pseudocode]{Evolutionary algorithm pseudocode}\label{alg:ea}
    \begin{algorithmic}[1]
    \State initialise($population$)
    \State evaluate($population$)
    \While{not termination condition}
        \State $parents$ $\gets$ select($population$)
        \State $offsprings$ $\gets$ crossover($parents$)
        \State mutate($offsprings$)
        \State evaluate($offsprings$)
        \State $population$ $\gets$ $offsprings$
    \EndWhile
    \State \Return bestof($population$)
    \end{algorithmic}
\end{algorithm}

There are different \textit{replacement and selection strategies}. We can either replace the whole population (generational replacement) or replace just some individuals in the population with the new offsprings (steady-state replacement). These two represent the tradeoff between exploration (we search for as many candidate solutions as possible) and elitism (we keep the well-performing candidate solutions in the population). Selection determines which individuals enter the crossover as parents. The goal of selection is to prefer the well-performing parents to push towards optimality but also to select some of the weaker individuals to push towards diversity. Roulette wheel selection chooses parents randomly proportionate to their fitness. Tournament selection gradually randomly samples a small batch of individuals and adds the best of them to the pool of parents until it's full.

Different problems can arise that primarily result in a stagnating population. The population stagnates if the best or mean fitness does not change and there is no diversity among the individuals causing the crossover happening between two same parents outputting the identical offspring. This can be partially solved by using a sufficiently explorative mutation that would introduce new individuals to the population. However, this might not help when the fitness of the stagnating population is much higher than the fitness of the newly introduced individual. As the selection procedures are fitness proportionate, it could easily happen that the new offspring would stay in the population just for one generation and would not help it to escape from the local optimum. Methods such as fitness sharing or speciation~\cite{sareni_fitness_1998} help to mitigate that by modifying the fitness of each individual so that it competes mainly with the individuals similar to it.

\subsubsection{Genetic programming} \label{sec:gp}

Genetic programming is a subfield of evolutionary algorithms concentrating on genotypes that are trees representing a hierarchical program or function~\cite{poli_field_2008}. The tree accepts input using its leaves and gradually propagates the signal to its root while applying a function or operator in each of the nodes that it is passing through. See Figure \ref{fig:gp_cross} for examples of functions represented using a tree data structure. A typical application of GP is symbolic regression, where the task is to find an analytic expression that fits the training data the best~\cite{kubalik_hybrid_2016}.

Genetic programming follows the same evolution scheme as a classical evolutionary algorithm (see Algorithm \ref{alg:ea}) but differs in the mutation and crossover operators. Subtree crossover selects a node (crossover point) in each of the parents and replaces the selected subtree from the first parent with a subtree from the second parent (see Figure \ref{fig:gp_cross}). Subtree mutation selects a node (mutation point) in the individual and replaces the subtree with a randomly generated tree. Point mutation selects a mutation node and changes its operator to a different operator of the same arity.

\begin{figure}[hbt]
    \centering
    \includegraphics[width=0.5\textwidth]{fig/subtree_crossover}
    \caption[Genetic programming: Subtree crossover]{Subtree crossover by~\cite{poli_field_2008}, CC BY-NC-ND 2.0 UK}
    \label{fig:gp_cross}
\end{figure}

The main challenge of genetic programming is handling code bloat. Code bloat refers to the excessive growth of the tree containing inviable code (a never reached branch) or unoptimised code (branch representing a formula reducible to a shorter expression). Bloat happens by gradually replacing subtrees with deeper subtrees using crossover or mutation, emerging from the fact that there are more deeper-level nodes than shallow-level nodes chosen for mutation/crossover. Growing the size of the tree makes it harder to create well-performing offsprings. To resolve that, we can introduce a reduction function that would compress the tree's redundant parts or limit the maximal depth of the tree.


\subsection{Neuroevolution background} \label{sec:neuroevolution}

This chapter describes the basics of neuroevolution, which is the core part of the thesis and reviews the relevant literature. We also review relevant tasks that are commonly solved by neuroevolution and available HyperNEAT software. This section is composed mainly of the information contained in survey/review articles on neuroevolution~\cite{dambrosio_hyperneat_2014},~\cite{stanley_designing_2019},~\cite{miikkulainen_neuroevolution_2020} and~\cite{del_ser_bio-inspired_2019}.

Neuroevolution is a subfield of artificial intelligence that interconnects the main concepts of evolutionary algorithms and deep learning. A typical goal of neuroevolution is to train a neural network using evolutionary optimisation algorithms. The optimisation focus ranges from the network architecture~\cite{sosa_deep_2018}, network parameters~\cite{such_deep_2018} or network hyperparameters such as learning scheme~\cite{jaderberg_population_2017}.

The basic unit over which the evolutionary algorithm operates is a population of individuals. Each individual is a genotype representing a neural network. There are two ways of encoding being used. \textit{Direct encoding}, where the neural network is specified explicitly by one to one mapping, and \textit{Indirect encoding}, where we specify how the network should be generated. The advantage of the direct encoding is its completeness and that we do not have to work with another level of abstraction. On the other hand, the indirect encoding allows more variability and a compact representation, which is crucial, especially in the case of neural networks. The neural network is referred to as \textit{phenotype}, while its encoded form is called a~\textit{genotype}.

The population of individuals (genotypes) is evolved by a classical evolutionary algorithm scheme. The mutation and crossover strategies directly depend on the chosen encoding strategy. As the crossover of neural networks is rather complicated~\cite{stanley_evolving_2002}, some of the authors~\cite{such_deep_2018} work with the mutation operator only.

A recent survey has shown that there is a big potential in neuroevolution~\cite{del_ser_bio-inspired_2019} and pointed out possible directions where the biologically inspired algorithms could go.

In 2018, Uber researchers released experiments~\cite{such_deep_2018} showing that even a simple genetic algorithm is able to outperform gradient-based (Q-learning), and gradient approximation based (Evolution strategies) methods in the reinforcement learning domain. Namely, they tested their implementation on Atari games, humanoid locomotion, and image maze domains. The same authors propose an indirect encoding method that recreates the neural network by applying mutations, specified by a vector of random mutation seeds, to the original network. The implementation of A. Ecoffet~\cite{ecoffet_paper_2018} shows the reproducibility of the results and points out problems related to the robustness of the solution that could be improved if better datasets were used for training.

In 2020, Google researchers released a study~\cite{tang_neuroevolution_2020} on indirect encoding of vision-based reinforcement learning tasks. This was achieved by introducing the concept of self-attention that could be intuitively interpreted as intentional blindness, which leads to simplification of the visual space. They used neuroevolution, more specifically CMA-ES algorithm, to train their network. The experimental results showed the potential to generalise with a compact encoding.

In the following text, we will describe the core neuroevolution approaches with a main focus on NEAT based methods, that are often referred to in the community.

\subsubsection{NEAT}

Neuroevolution of Augmenting Topologies (NEAT) addresses how to evolve both network topology and the parameters simultaneously. The authors~\cite{stanley_evolving_2002} introduce the following innovations:

\begin{itemize}
    \item crossover of different topologies
    \item specification for protecting different topology types
    \item initialisation with minimal structures
\end{itemize}

Specification protects the fitness diversity in the population which prevents stagnation. Minimal initialisation ensures that the solution complexity grows from the smallest structures to the most complex. This increases the probability that the best-performing individual is as simple as possible. If the individual gets too complex, it gets harder to recombine or mutate it into a meaningful solution.

The crossover innovation addresses the issue of competing conventions (permutation problem). Traditionally, crossover is problematic in subgraph swapping methods (see Figure \ref{fig:gp_cross}) as it is challenging to recognise homology between different networks. If the permutation problem is not handled, crossovers of two homological individuals, as shown in Figure \ref{fig:competing}, degrades the performance of the network.

\begin{figure}[hbt]
    \centering
    \includegraphics[width=0.7\textwidth]{fig/competing.pdf}
    \caption[NEAT: Competing conventions problem]{Competing conventions problem. Crossover is performed over two same networks, and the resulting solution loses a key feature as it was not identified that the networks are in fact the same. Both of the networks contain features 2,3,4; but when we recombine the two homological networks, the resulting network is either missing information from feature 2 or feature 4, while no new feature is added. Image adapted from~\cite{stanley_evolving_2002}.}
    \label{fig:competing}
\end{figure}

In NEAT, this problem is solved by a special kind of direct encoding (see Figure \ref{fig:neat}) that tracks the history of modifications applied to the network and, therefore, can recognise when two networks were created in the same way. Two networks with the same history have the same topology, but not necessarily the same weights. To perform such tracking and to keep the networks minimal, the algorithm starts by evolving simple neural networks with no hidden nodes, rather than starting with a population of randomly generated networks.

\begin{figure}[hbt]
    \centering
    \includegraphics[height=0.3\textheight]{fig/neat_crossover.pdf}
    \caption[NEAT: crossover of two genomes]{NEAT crossover of two genomes and their network representation. Each genome consists of list of connections, their unique innovation number and whether they are enabled or not.}
    \label{fig:neat}
\end{figure}

During the crossover phase, the genomes are compared gene-vice, and matching genes are selected randomly from any parent, while disjoint genes are selected from the better parent. The concept of disjoint/compatible genes is also used in the fitness sharing function, ensuring that the diversity in the population is kept.

Even though NEAT experiences scalability problems induced by direct encoding, it is key research on which the more recent research builds. One of the recent papers, that still feature NEAT (though only partially) is~\cite{peng_neat_2018}. The authors solve a RL task by strictly separating policy and feature learning (algorithm NEAT+PGS). While the policy evolves by Policy Gradient Search, the features are evolved by NEAT algorithm.

\subsubsection{Population Based Training}

Another direction of neuroevolution in recent years is Population Based Training (PBT) by Jaderberg, et al.~\cite{jaderberg_population_2017}. PBT jointly optimises weights and hyperparameters (learning rate, and entropy cost) of the network to reduce the cost of ML model deployment. An asynchronous optimisation model is used to utilise computational resources effectively. The final output of the algorithm is a network with fixed architecture, and a schedule of hyperparameters (i.e.,~if we want to retrain the network later, the hyperparameters dynamically change during the training).

Firstly, for each individual in the initial population, the parameter vector (network weights) is updated using a gradient descent method. Then, there is an exploit phase, where given the fitness of the individual and the rest of the population, the algorithm either keeps the current solution or accepts the weights of the best performing model. The individual then continues to an explore phase, where the hyperparameters are changed to suit the new parameter vector better. These three steps (SGD update, exploit, explore) are repeated until the training ends. The best performing network is selected.

In Li, et al.~\cite{li_generalized_2019}, PBT is further extended to perform black-box optimisation, which means that no assumptions on model architecture, loss function, and optimisation scheme have to be made. The algorithm is based on Vizier, a hyperparameter optimisation service~\cite{golovin_google_2017} and the PBT optimisation algorithm, the difference is that the mutation is not performed inside the worker, but in the supervising controller which allows more variability.

%\subsection{NES}
% Wierstra, D. et al. Natural evolution strategies. J. Mach. Learn. Res. 15, 949–980 (2014).

\subsection{HyperNEAT}

HyperNEAT addresses the scalability problems of approaches with direct representation by introducing a concept of Compositional Pattern Producing Networks (CPPN) to manage indirect encoding of the neural networks. In HyperNEAT, one individual is not the network itself but a CPPN that generates it. These CPPNs are then optimised using the original NEAT algorithm (see Algorithm \ref{alg:hn}).

\begin{figure}[hbt]
    \centering
    \includegraphics[width=0.95\textwidth]{fig/gen_to_phen_v3.pdf}
    \caption[HyperNEAT: genotype to phenotype mapping]{Genotype (CPPN) to phenotype (neural network) mapping. Firstly, every possible connection of a pair of nodes in the substrate is queried. Secondly, the coordinates of the pair of nodes are the input of the CPPN network. Lastly, CPPN outputs the weight of the connection between the pair of nodes.
    }
    \label{fig:hyperneat}
\end{figure}

CPPN works over a predefined grid called a substrate. Such substrate could be a cube with regularly spaced nodes where each node is connected with its direct neighbours; or a~cube divided into layers where the connections are directed from layer $i$ to layer $i+1$. CPPN is a function that accepts coordinates of two nodes in the substrate and returns the weight of their connection. The whole network is built by querying all the allowed connections. While the network structure is limited by the structure of the underlying substrate, it can still learn to produce many different architectures if we consider that the connections with low weights are not created in the network. See the process of converting CPPN phenotype into the neural network with initialised weights in Figure \ref{fig:hyperneat}.

\begin{algorithm}[th]
    \caption[HyperNEAT pseudocode]{HyperNEAT pseudocode adapted from~\cite{stanley_hypercube-based_2009}}\label{alg:hn}
    \begin{algorithmic}[1]
    \State choose substrate configuration (node layout)
    \State initialise population with randomised minimal CPPNs
    \While{not termination condition}
        \For{each individual in the population}
            \State query its CPPN for connection weight of each possible connection in the substrate
            \State run the created neural network to obtain fitness
        \EndFor
        \State reproduce CPPNs according to NEAT method to get updated population
    \EndWhile
    \State \Return bestof($population$)
    \end{algorithmic}
\end{algorithm}






There are two main advantages of the indirect representation. Firstly, it is able to generalise to different substrate sizes, which can be especially useful if the size of the input (data sample) or output is variable. Secondly, the representation is compact, allowing to represent thousands of neural network parameters only by dozens of CPPN nodes.

While HyperNEAT has been criticised by van den Berg and Whiteson~\cite{van_den_berg_critical_2013} for not performing well on irregular tasks, the authors of HyperNEAT have shown~\cite{stanley_cppns_2013} counterexamples to disprove it. Several years later, a survey~\cite{dambrosio_hyperneat_2014} on the progress of HyperNEAT and its extensions and applications was published. The findings are presented in the next paragraphs.


\textit{Adaptive HyperNEAT} (Risi \& Stanley~\cite{risi_indirectly_2010}) has focused on the plasticity of the networks, i.e. the ability  to evolve weights. This improvement helps to circumvent the weight limitations imposed by the nature of the CPPN.

\textit{HybrID} (Clune, et al.~\cite{clune_performance_2011}) starts with indirect encoding and then continues with direct encoding to allow individual weight fine-tuning.

\textit{HyperNEAT-LEO} (Verbancsics \& Stanley~\cite{verbancsics_constraining_2011}) addresses the issue of disappearing connections by adding an output to CPPN that determines whether the connection should be present or not. This improvement helps to distinguish which connections are in the network, and which connections are below a threshold.

\textit{ES-HyperNEAT} (Risi \& Stanley~\cite{risi_enhanced_2012}) adds the possibility to evolve position and number of hidden layers in the substrate. This improvement allows the substrate to evolve with the CPPN, and not to remain prefixed by the user.

\textit{HyperGP} (Buk~\cite{buk_neat_2009}) uses genetic programming instead of NEAT to evolve the CPPN.

\textit{Deep HyperNEAT} (Sosa \& Stanley~\cite{sosa_deep_2018}) is an extension of NEAT where one node in a genotype does not represent a neural network node but a neural network layer and its specifications. Fitness is based on how well the network can be trained in a few generations.

\textit{CoDeepNEAT} (Miikkulainen, et al.~\cite{miikkulainen_evolving_2017}, and Bohrer, et al.~\cite{bohrer_neuroevolution_2020}) is similar to Deep HyperNEAT, but instead of having neural network architecture as a population, we evolve two different populations -- blueprints and modules. Modules are small parts of an architecture, and blueprints are the recipes that assemble them.

\subsubsection{Available HyperNEAT implementations}

As the goal of this thesis is to combine NEAT-based algorithms with SGD, we implement the code in Python as it is currently the most used open-source machine learning language with major deep learning libraries (Keras, PyTorch), data handling libraries (Pandas, Seaborn) and RL experimental platforms (OpenAI gym). Therefore, I focus on the libraries implemented in Python. However, there are also other (Hyper)NEAT libraries, especially for C\# and C++. These implementations can be found in the software list maintained by the EPLEX group at UCF (\url{http://eplex.cs.ucf.edu/software-list}).

While there is plenty of available implementations of NEAT-based algorithms, it is challenging to distinguish between them, and choose the right one to use it 'as a tool' since most of the published implementations are paper-/project- related, and not maintained or not general enough.

\textit{NEAT} is implemented by McIntyre, et all. in~\cite{mcintyre_neat-python_2019} and indexed in PyPI as \texttt{neat-python}. This implementation looks stable, tested and quite well documented. It is not further developed, but it is still maintained. This is also the NEAT implementation that is often built upon in the research papers.

\textit{PyTorch based} implementation is provided by Uber research~\cite{uber_research_pytorch-neat_2018}. This implementation covers NEAT, HyperNEAT, and Adaptive HyperNEAT. The interface looks general enough. However, it seems that it is not further maintained as the reported issues are not resolved.

\textit{Tensorflow based} implementation is made by Bodnar~\cite{bodnar_tensorflow-neat_2018}. However, this implementation is based on Tensorflow 1.x, while the current standard is Tensorflow 2.x, and as the author states in his technical report, the implementation is significantly slower than the PyTorch version.

Another \textit{Tensorflow based} implementation is done by Uber research~\cite{uber_research_deep-neuroevolution_2019}. This implementation supports GPU evaluation and parallelisation. However, only a simple genetic algorithm and algorithm for evolution strategies is implemented. The project is labelled as work in progress, but the last update was done one and a half years ago. Even though the code might not be applicable as is at the moment, it could be worth it to improve it as GPU supported operations, and a good parallelisation could significantly speed up the runtime of the experiments.

\subsection{Use cases of NEAT based methods}

Generally, HyperNEAT-based methods are used mainly for tasks with significant geometrical features (e.g. symmetry, repetition) or for tasks where varying input/output size handling is required (i.e. image resolution). Such features are often found in robot control, image recognition, and reinforcement learning tasks.

\subsubsection{Vision}

The power of CPPNs to create geometrical objects is shown in Clune \& Lipson~\cite{clune_evolving_2011} where 3D objects are designed and evolved. The paper is accompanied by a popularisation website \url{http://endlessforms.com/}.

Calimeri, et al.~\cite{calimeri_blood_2019} show the application of HyperNEAT in the biomedical domain for blood vessel segmentation and compare the method with other state-of-the-art methods for image segmentation. Verbancsics \& Harguess~\cite{verbancsics_feature_2015} use HyperNEAT to classify maritime vessels from satellite images. The main presented  advantage of using HyperNeat is the ability to work with varying scales of images. CoDeepNEAT is used in Miikkulainen, et al.~\cite{miikkulainen_evolving_2017} to create image captioning of a major online magazine. The authors use image and text representation of the items to produce captions for blind people.

Neurogram is an online tool (\url{https://otoro.net/neurogram/}) by Ha~\cite{ha_neurogram_2015} that implements the NEAT algorithm to allow the user to experiment with NEAT operators on random images. Picbreeder is an online community-based tool (\url{http://www.picbreeder.org/}) by Secretan, et al.,~\cite{secretan_picbreeder_2011} which evolves art using the NEAT algorithm based on user experience. The users can create new art pieces by evolving existing pictures or combining them with their own art.


\subsubsection{Games}

There has been a lot of research applying neuroevolutionary algorithms in Atari gaming. This domain is of particular interest as it is defined in a very convenient open-source simulator OpenAI Gym~\cite{brockman_openai_2016}, and provides several complex environments corresponding to different Atari games. To play Atari, one has to be able to map a quickly changing screen capture to actions. As the different environments are similar but still different enough, the domain is often used to test the ability to generalise.

Hausknecht, et al.~\cite{hausknecht_hyperneat-ggp_2012} use HyperNEAT; Such, et al.~\cite{such_deep_2018} use a simple version of a genetic algorithm without mutation; and Peng, et al.~\cite{peng_neat_2018} use NEAT+PGS to create a potentially general Atari player. This list is not exhaustive as this domain is very popular in Reinforcement learning research as well. Recently, Atari ZOO~\cite{such_atari_2019} was published to allow easier comparison between the implemented algorithms.

Schrum~\cite{schrum_evolving_2018} presents a HyperNEAT-based algorithm specialised to generate CNN-like architectures. The author shows the results using the Tetris game puzzle.



\subsubsection{Robot control} % pridat uvodni robo clanky od Clune, Stanley

Cheney, et al.~\cite{cheney_unshackling_2013} evolve soft robots to create new morphologies. They utilise HyperNEAT to create morphologies of varying density. Lee, et al. evolve robot gait~\cite{lee_evolving_2013} using HyperNEAT for a 4-legged robot. Drchal, et al.~\cite{drchal_hyperneat_2009} use HyperNEAT for a line following robot reacting to variable size sensor inputs.

Buk~\cite{buk_neat_2009} uses HyperGP to control robots using their sensoric inputs. The author employs the indirect encoding concept of CPPN while disregarding the complexities of the NEAT algorithm, and using genetic programming instead. Dvorský~\cite{dvorsky_neuroevolutionary_2013} then uses HyperGP in his master thesis to control multi-legged robots.

Haasdijk, et al.~\cite{haasdijk_hyperneat_2010} control a multi-robot organism by HyperNEAT. The authors leverage the geometric aspects of HyperNEAT to create an algorithm that allows each of the robots to operate autonomously.




\subsubsection{Other}


Bahçeci \& Miikkulainen~\cite{bahceci_transfer_2008} explore the possibilities to transfer heuristics created for simple board games and use them as a hot start for more complex games. Didi~\cite{didi_multi-agent_2016} extends HyperNEAT for a policy transfer task with experiments completed in keep-away RoboCup soccer domain.

Boyles~\cite{boyles_evolving_2015} evolves scouts for military simulations using the NEAT algorithm in his master thesis. Kroos \& Plumbley~\cite{kroos_neuroevolution_2017} extends NEAT to use it for sound event detection where the main goal is to develop a competent network that is as minimal as possible.





