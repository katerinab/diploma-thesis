from dataclasses import dataclass


@dataclass
class Substrate:
	num_layers: int
	num_nodes_in_layers: int
	num_inputs: int
	num_outputs: int
