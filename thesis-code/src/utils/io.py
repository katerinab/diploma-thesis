from pathlib import Path


def create_dir_and_parents_if_not_exist(path: Path):
    try:
        path.mkdir(parents=True, exist_ok=False)
        print(f'Created folder {path=} already exists')
    except FileExistsError:
        print(f'Folder {path=} already exists')
