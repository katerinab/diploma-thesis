from __future__ import division, print_function

from itertools import count
from typing import Dict

from neat.activations import ActivationFunctionSet
from neat.aggregations import AggregationFunctionSet
from neat.config import ConfigParameter, write_pretty_params
from neat.genes import DefaultConnectionGene, DefaultNodeGene
from neat.genome import DefaultGenome

from algorithms.hyperneat_classes.genome_handler import GenomeHandler, GenomeType


class MultiGenomeConfig:
    """
    Adapted from: https://github.com/CodeReclaimers/neat-python/blob/master/neat/genome.py
    Extends the default genome to be able to represent more CPPNs in one genome.
    Sets up and holds configuration information for the DefaultGenome class.
    """
    allowed_connectivity = ['unconnected', 'fs_neat_nohidden', 'fs_neat', 'fs_neat_hidden',
                            'full_nodirect', 'full', 'full_direct',
                            'partial_nodirect', 'partial', 'partial_direct']

    def __init__(self, params):
        # Create full set of available activation functions.
        self.activation_defs = ActivationFunctionSet()
        # ditto for aggregation functions - name difference for backward compatibility
        self.aggregation_function_defs = AggregationFunctionSet()
        self.aggregation_defs = self.aggregation_function_defs

        self._params = [ConfigParameter('num_inputs', int),
                        ConfigParameter('num_hidden', int),
                        ConfigParameter('feed_forward', bool),
                        ConfigParameter('compatibility_disjoint_coefficient', float),
                        ConfigParameter('compatibility_weight_coefficient', float),
                        ConfigParameter('conn_add_prob', float),
                        ConfigParameter('conn_delete_prob', float),
                        ConfigParameter('node_add_prob', float),
                        ConfigParameter('node_delete_prob', float),
                        ConfigParameter('single_structural_mutation', bool, 'false'),
                        ConfigParameter('structural_mutation_surer', str, 'default'),
                        ConfigParameter('initial_connection', str, 'unconnected')]

        # Gather configuration data from the gene classes.
        self.node_gene_type = params['node_gene_type']
        self._params += self.node_gene_type.get_config_params()
        self.connection_gene_type = params['connection_gene_type']
        self._params += self.connection_gene_type.get_config_params()

        # Use the configuration data to interpret the supplied parameters.
        for p in self._params:
            setattr(self, p.name, p.interpret(params))

        # By convention, input pins have negative keys, and the output
        # pins have keys 0,1,...
        self.input_keys = [-i - 1 for i in range(self.num_inputs)]

        self.connection_fraction = None

        # Verify that initial connection type is valid.
        # pylint: disable=access-member-before-definition
        if 'partial' in self.initial_connection:
            c, p = self.initial_connection.split()
            self.initial_connection = c
            self.connection_fraction = float(p)
            if not (0 <= self.connection_fraction <= 1):
                raise RuntimeError(
                    "'partial' connection value must be between 0.0 and 1.0, inclusive.")

        assert self.initial_connection in self.allowed_connectivity

        # Verify structural_mutation_surer is valid.
        # pylint: disable=access-member-before-definition
        if self.structural_mutation_surer.lower() in ['1', 'yes', 'true', 'on']:
            self.structural_mutation_surer = 'true'
        elif self.structural_mutation_surer.lower() in ['0', 'no', 'false', 'off']:
            self.structural_mutation_surer = 'false'
        elif self.structural_mutation_surer.lower() == 'default':
            self.structural_mutation_surer = 'default'
        else:
            error_string = "Invalid structural_mutation_surer {!r}".format(
                self.structural_mutation_surer)
            raise RuntimeError(error_string)

        self.node_indexer = None

    def set_cppn_type(self, genome_type: GenomeType):
        self.genome_handler: GenomeHandler = genome_type.genome_handler_creator()
        self.num_outputs = self.genome_handler.num_cppn_outputs
        self.output_keys = [i for i in range(self.num_outputs)]

    def add_activation(self, name, func):
        self.activation_defs.add(name, func)

    def add_aggregation(self, name, func):
        self.aggregation_function_defs.add(name, func)

    def save(self, f):
        if 'partial' in self.initial_connection:
            if not (0 <= self.connection_fraction <= 1):
                raise RuntimeError(
                    "'partial' connection value must be between 0.0 and 1.0, inclusive.")
            f.write('initial_connection      = {0} {1}\n'.format(self.initial_connection,
                                                                 self.connection_fraction))
        else:
            f.write('initial_connection      = {0}\n'.format(self.initial_connection))

        assert self.initial_connection in self.allowed_connectivity

        write_pretty_params(f, self, [p for p in self._params
                                      if 'initial_connection' not in p.name])

    def get_new_node_key(self, node_dict):
        if self.node_indexer is None:
            self.node_indexer = count(max(list(node_dict)) + 1)

        new_id = next(self.node_indexer)

        assert new_id not in node_dict

        return new_id

    def check_structural_mutation_surer(self):
        if self.structural_mutation_surer == 'true':
            return True
        elif self.structural_mutation_surer == 'false':
            return False
        elif self.structural_mutation_surer == 'default':
            return self.single_structural_mutation
        else:
            error_string = "Invalid structural_mutation_surer {!r}".format(
                self.structural_mutation_surer)
            raise RuntimeError(error_string)


class MultiGenome:
    """
    Adapted from: https://github.com/CodeReclaimers/neat-python/blob/master/neat/genome.py
    Extends the default genome to be able to represent more CPPNs in one genome.
    The individual operators are analogous to the default genome, the MultiGenome is basically a list of default
    genomes.

    A genome for generalized neural networks.

    Terminology
        pin: Point at which the network is conceptually connected to the external world;
             pins are either input or output.
        node: Analog of a physical neuron.
        connection: Connection between a pin/node output and a node's input, or between a node's
             output and a pin/node input.
        key: Identifier for an object, unique within the set of similar objects.

    Design assumptions and conventions.
        1. Each output pin is connected only to the output of its own unique
           neuron by an implicit connection with weight one. This connection
           is permanently enabled.
        2. The output pin's key is always the same as the key for its
           associated neuron.
        3. Output neurons can be modified but not deleted.
        4. The input values are applied to the input pins unmodified.
    """

    @classmethod
    def parse_config(cls, param_dict):
        param_dict['node_gene_type'] = DefaultNodeGene
        param_dict['connection_gene_type'] = DefaultConnectionGene
        return MultiGenomeConfig(param_dict)

    @classmethod
    def write_config(cls, f, config):
        config.save(f)

    def __init__(self, key, config):
        # Unique identifier for a genome instance.
        self.key = key

        self.subgenomes: Dict[int, DefaultGenome] = dict()
        self.genome_handler = config.genome_handler
        self.num_subgenomes = self.genome_handler.num_cppn_subgenomes
        for i in range(self.num_subgenomes):
            # (gene_key, gene) pairs for gene sets.
            self.subgenomes[i] = DefaultGenome(self.key)

        # Fitness results.
        self.fitness = None

    def set_fitness(self, fitness: float):
        self.fitness = fitness
        for subgenome in self.subgenomes.values():
            subgenome.fitness = fitness

    def configure_new(self, config):
        """Configure a new genome based on the given configuration."""
        for subgenome in self.subgenomes.values():
            subgenome.configure_new(config)

    def configure_crossover(self, genome1, genome2, config):
        """ Configure a new genome by crossover from two parent genomes. """
        for subgenome1, subgenome2, cur_subgenome in zip(genome1.subgenomes.values(),
                                                         genome2.subgenomes.values(),
                                                         self.subgenomes.values()):
            subgenome1.fitness = genome1.fitness
            cur_subgenome.configure_crossover(subgenome1, subgenome2, config)

    def mutate(self, config):
        """ Mutates this genome. """
        for subgenome in self.subgenomes.values():
            subgenome.mutate(config)

    def distance(self, other, config) -> float:
        """
        Returns the genetic distance between this genome and the other. This distance value
        is used to compute genome compatibility for speciation.
        """
        distance = 0.0
        for subgenome1, subgenome2, in zip(self.subgenomes.values(), other.subgenomes.values()):
            distance += subgenome1.distance(subgenome2, config)
        return distance / self.num_subgenomes

    def size(self):
        """
        Returns genome 'complexity', taken to be
        (number of nodes, number of enabled connections)
        """
        num_nodes, num_enabled_connections = 0.0, 0.0
        for subgenome in self.subgenomes.values():
            cur_nodes, cur_conn = subgenome.size()
            num_nodes += cur_nodes
            num_enabled_connections += cur_conn
        return num_nodes, num_enabled_connections

    def __str__(self):
        s = f"-----------------------------\nIndividual {self.key}\n"
        for sub_name, subgenome in self.subgenomes.items():
            s += subgenome.__str__() + "\n"
        s += "-----------------------------"
        return s
