from typing import Any, Dict, Optional, Tuple, Type, Union

import numpy as np
import torch as th
from stable_baselines3 import DQN
from stable_baselines3.common import logger
from stable_baselines3.common.type_aliases import GymEnv, Schedule
from stable_baselines3.dqn.policies import DQNPolicy
from torch.nn import functional as F

from algorithms.hyperneat_classes.feed_forward_network import FeedForwardNetwork


class CustomDQN(DQN):
    """
    Custom Deep Q-Network (DQN) - adapted from Stable Baseline3
    - extension for external policy

    Paper: https://arxiv.org/abs/1312.5602, https://www.nature.com/articles/nature14236
    Default hyperparameters are taken from the nature paper,
    except for the optimizer and learning rate that were taken from Stable Baselines defaults.

    :param policy: The policy model to use (MlpPolicy, CnnPolicy, ...)
    :param env: The environment to learn from (if registered in Gym, can be str)
    :param learning_rate: The learning rate, it can be a function
        of the current progress remaining (from 1 to 0)
    :param buffer_size: size of the replay buffer
    :param learning_starts: how many steps of the model to collect transitions for before learning starts
    :param batch_size: Minibatch size for each gradient update
    :param tau: the soft update coefficient ("Polyak update", between 0 and 1) default 1 for hard update
    :param gamma: the discount factor
    :param train_freq: Update the model every ``train_freq`` steps. Alternatively pass a tuple of frequency and unit
        like ``(5, "step")`` or ``(2, "episode")``.
    :param gradient_steps: How many gradient steps to do after each rollout (see ``train_freq``)
        Set to ``-1`` means to do as many gradient steps as steps done in the environment
        during the rollout.
    :param optimize_memory_usage: Enable a memory efficient variant of the replay buffer
        at a cost of more complexity.
        See https://github.com/DLR-RM/stable-baselines3/issues/37#issuecomment-637501195
    :param target_update_interval: update the target network every ``target_update_interval``
        environment steps.
    :param exploration_fraction: fraction of entire training period over which the exploration rate is reduced
    :param exploration_initial_eps: initial value of random action probability
    :param exploration_final_eps: final value of random action probability
    :param max_grad_norm: The maximum value for the gradient clipping
    :param tensorboard_log: the log location for tensorboard (if None, no logging)
    :param create_eval_env: Whether to create a second environment that will be
        used for evaluating the agent periodically. (Only available when passing string for the environment)
    :param policy_kwargs: additional arguments to be passed to the policy on creation
    :param verbose: the verbosity level: 0 no output, 1 info, 2 debug
    :param seed: Seed for the pseudo random generators
    :param device: Device (cpu, cuda, ...) on which the code should be run.
        Setting it to auto, the code will be run on the GPU if possible.
    :param _init_setup_model: Whether or not to build the network at the creation of the instance
    :param external_policy: Policy calculated outside of the DQN framework used for DQN training (Q target policy),
    :param external_policy_threshold_time_steps: number of time steps in the beginning of training when external policy
    is used instead of Q target to guide true Q-value
    :param use_external_policy: Whether to use external policy in DQN instead of Q target network
    """

    def __init__(self, policy: Union[str, Type[DQNPolicy]], env: Union[GymEnv, str],
                 learning_rate: Union[float, Schedule] = 1e-4, buffer_size: int = 1000000, learning_starts: int = 50000,
                 batch_size: Optional[int] = 32, tau: float = 1.0, gamma: float = 0.99,
                 train_freq: Union[int, Tuple[int, str]] = 4, gradient_steps: int = 1,
                 optimize_memory_usage: bool = False, target_update_interval: int = 10000,
                 exploration_fraction: float = 0.1, exploration_initial_eps: float = 1.0,
                 exploration_final_eps: float = 0.05, max_grad_norm: float = 10, tensorboard_log: Optional[str] = None,
                 create_eval_env: bool = False, policy_kwargs: Optional[Dict[str, Any]] = None, verbose: int = 0,
                 seed: Optional[int] = None, device: Union[th.device, str] = "auto", _init_setup_model: bool = True,
                 external_policy: FeedForwardNetwork = None, external_policy_threshold_time_steps: int = None,
                 use_external_policy: bool = False):
        super().__init__(policy, env, learning_rate, buffer_size, learning_starts, batch_size, tau, gamma, train_freq,
                         gradient_steps, optimize_memory_usage, target_update_interval, exploration_fraction,
                         exploration_initial_eps, exploration_final_eps, max_grad_norm, tensorboard_log,
                         create_eval_env, policy_kwargs, verbose, seed, device, _init_setup_model)
        self.use_external_policy = use_external_policy
        self.external_policy = external_policy
        self.external_policy_threshold_time_steps = external_policy_threshold_time_steps

    def train(self, gradient_steps: int, batch_size: int = 100) -> None:
        """
        Sample the replay buffer and do the updates
        (gradient descent and update target networks)
        """
        # Update learning rate according to schedule
        self._update_learning_rate(self.policy.optimizer)

        losses = []
        for gradient_step in range(gradient_steps):
            # Sample replay buffer
            replay_data = self.replay_buffer.sample(batch_size, env=self._vec_normalize_env)

            with th.no_grad():
                # Compute the next Q-values using the target network
                # Follow greedy policy: use the one with the highest value
                if self.use_external_policy:
                    next_q_values = self.q_net(replay_data.next_observations)
                    external_q_vals = self.external_policy.forward(replay_data.next_observations)
                    actions = external_q_vals.argmax(dim=1)
                    next_q_values = next_q_values.gather(1, actions.view(-1, 1))
                else:
                    next_q_values = self.q_net_target(replay_data.next_observations)
                    next_q_values, _ = next_q_values.max(dim=1)
                # Avoid potential broadcast issue
                next_q_values = next_q_values.reshape(-1, 1)
                # 1-step TD target
                target_q_values = replay_data.rewards + (1 - replay_data.dones) * self.gamma * next_q_values

            # Get current Q-values estimates
            current_q_values = self.q_net(replay_data.observations)

            # Retrieve the q-values for the actions from the replay buffer
            current_q_values = th.gather(current_q_values, dim=1, index=replay_data.actions.long())

            # Compute Huber loss (less sensitive to outliers)
            loss = F.smooth_l1_loss(current_q_values, target_q_values)
            losses.append(loss.item())

            # Optimize the policy
            self.policy.optimizer.zero_grad()
            loss.backward()
            # Clip gradient norm
            th.nn.utils.clip_grad_norm_(self.policy.parameters(), self.max_grad_norm)
            self.policy.optimizer.step()

        # Increase update counter
        self._n_updates += gradient_steps
        if self.num_timesteps > self.external_policy_threshold_time_steps:
            self.use_external_policy = False

        logger.record("train/n_updates", self._n_updates, exclude="tensorboard")
        logger.record("train/loss", np.mean(losses))
