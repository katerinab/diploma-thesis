from enum import Enum
from typing import Dict, Optional

from algorithms.hyperneat_classes.cppn_network import CppnNetwork
from algorithms.hyperneat_classes.substrate import Substrate


class GenomeHandler:
    """
    Helper tool for different genome types.
    Selects the correct cppn from the cppn dictionary and returns the cppn name.
    :param genome_type: The type of the genome
    """

    def __init__(self, genome_type):
        self.genome_type = genome_type
        self.num_cppn_subgenomes: Optional[int] = None
        self.subgenomes_names: Dict[int, str] = dict()
        self.use_bias: Optional[bool] = None
        self.num_cppn_outputs: Optional[int] = None

    def weight_bias_selector(self, cppns: Dict[int, CppnNetwork], subs: Substrate, l1: int,
                             l2: Optional[int]) -> CppnNetwork:
        """
        Selects the correct cppn from the cppn dictionary.
        :param cppns: dictionary of CPPNs for given genome
        :param subs: substrate of the underlying neural network
        :param l1: first layer
        :param l2: second layer
        :return: the selected cppn
        """
        pass

    def get_cppn_name(self, cppn_key: int) -> Optional[str]:
        """
        Given cppn key, return its name
        :param cppn_key: cppn key
        :return: cppn name
        """
        return self.subgenomes_names.get(cppn_key)


class WeightBiasHandler(GenomeHandler):
    def __init__(self, genome_type):
        super().__init__(genome_type)
        self.subgenomes_names: Dict[int, str] = {0: "weight", 1: "bias"}
        self.num_cppn_subgenomes = 2
        self.use_bias = True
        self.num_cppn_outputs = 1

    def weight_bias_selector(self, cppns: Dict[int, CppnNetwork], subs: Substrate, l1: int,
                             l2: Optional[int]) -> CppnNetwork:
        assert len(
            cppns) == self.num_cppn_subgenomes, f"Number of CPPNs must be {self.num_cppn_subgenomes} but is {len(cppns)}"
        if l2 is None:
            return cppns.get(1)
        else:
            return cppns.get(0)


class JointWeightBiasHandler(GenomeHandler):
    def __init__(self, genome_type):
        super().__init__(genome_type)
        self.subgenomes_names: Dict[int, str] = {0: "weight+bias"}
        self.num_cppn_subgenomes = 1
        self.use_bias = True
        self.num_cppn_outputs = 1

    def weight_bias_selector(self, cppns: Dict[int, CppnNetwork], subs: Substrate, l1: int,
                             l2: Optional[int]) -> CppnNetwork:
        assert len(
            cppns) == self.num_cppn_subgenomes, f"Number of CPPNs must be {self.num_cppn_subgenomes} but is {len(cppns)}"
        return cppns.get(0)


class WeightHandler(GenomeHandler):
    def __init__(self, genome_type):
        super().__init__(genome_type)
        self.subgenomes_names = {0: "weight"}
        self.num_cppn_subgenomes = 1
        self.use_bias = False
        self.num_cppn_outputs = 1

    def weight_bias_selector(self, cppns: Dict[int, CppnNetwork], subs: Substrate, l1: int,
                             l2: Optional[int]) -> CppnNetwork:
        assert len(
            cppns) == self.num_cppn_subgenomes, f"Number of CPPNs must be {self.num_cppn_subgenomes} but is {len(cppns)}"
        return cppns.get(0)


class JointWeightBiasWithConnectionSwitchHandler(GenomeHandler):
    def __init__(self, genome_type):
        super().__init__(genome_type)
        self.subgenomes_names: Dict[int, str] = {0: "weight+bias"}
        self.num_cppn_subgenomes = 1
        self.use_bias = True
        self.num_cppn_outputs = 2

    def weight_bias_selector(self, cppns: Dict[int, CppnNetwork], subs: Substrate, l1: int,
                             l2: Optional[int]) -> CppnNetwork:
        assert len(
            cppns) == self.num_cppn_subgenomes, f"Number of CPPNs must be {self.num_cppn_subgenomes} but is {len(cppns)}"
        return cppns.get(0)


class WeightBiasWithConnectionSwitchHandler(GenomeHandler):
    def __init__(self, genome_type):
        super().__init__(genome_type)
        self.subgenomes_names: Dict[int, str] = {0: "weight", 1: "bias"}
        self.num_cppn_subgenomes = 2
        self.use_bias = True
        self.num_cppn_outputs = 2

    def weight_bias_selector(self, cppns: Dict[int, CppnNetwork], subs: Substrate, l1: int,
                             l2: Optional[int]) -> CppnNetwork:
        assert len(
            cppns) == self.num_cppn_subgenomes, f"Number of CPPNs must be {self.num_cppn_subgenomes} but is {len(cppns)}"
        if l2 is None:
            return cppns.get(1)
        else:
            return cppns.get(0)


class WeightWithConnectionSwitchHandler(GenomeHandler):
    def __init__(self, genome_type):
        super().__init__(genome_type)
        self.subgenomes_names = {0: "weight"}
        self.num_cppn_subgenomes = 1
        self.use_bias = False
        self.num_cppn_outputs = 2

    def weight_bias_selector(self, cppns: Dict[int, CppnNetwork], subs: Substrate, l1: int,
                             l2: Optional[int]) -> CppnNetwork:
        assert len(
            cppns) == self.num_cppn_subgenomes, f"Number of CPPNs must be {self.num_cppn_subgenomes} but is {len(cppns)}"
        return cppns.get(0)


class GenomeType(Enum):
    WB_2 = 'CPPN(weights+bias)_2out'
    W_B_2 = 'CPPN(weights,bias)_2out'
    W_2 = 'CPPN(weights)_2out'
    WB_1 = 'CPPN(weights+bias)_1out'
    W_B_1 = 'CPPN(weights,bias)_1out'
    W_1 = 'CPPN(weights)_1out'

    def genome_handler_creator(self):
        if self == GenomeType.WB_1:
            return JointWeightBiasHandler(self)
        if self == GenomeType.W_B_1:
            return WeightBiasHandler(self)
        elif self == GenomeType.W_1:
            return WeightHandler(self)
        elif self == GenomeType.WB_2:
            return JointWeightBiasWithConnectionSwitchHandler(self)
        elif self == GenomeType.W_B_2:
            return WeightBiasWithConnectionSwitchHandler(self)
        elif self == GenomeType.W_2:
            return WeightWithConnectionSwitchHandler(self)
        assert f"Correct CPPN SETTING must be selected, {self.value} is not a valid option"
