import numpy as np


def concatenate_per_row(A, B):
    """
    Source: https://stackoverflow.com/a/41590049
    """
    m1, n1 = A.shape
    m2, n2 = B.shape

    out = np.zeros((m1, m2, n1 + n2), dtype=A.dtype)
    out[:, :, :n1] = A[:, None, :]
    out[:, :, n1:] = B
    return out.reshape(m1 * m2, -1)
