from time import time

import torch
from future.utils import iteritems
from stable_baselines3 import DQN
from torch.utils.data import TensorDataset, DataLoader

from algorithms.dqn import Dqn
from algorithms.hyperneat import HyperNEAT
from algorithms.hyperneat_dqn_joined import JoinedHyperneatDqn
from algorithms.neat_extensions.multigenome import MultiGenome
from algorithms.hyperneat_classes.cppn_network import CppnNetwork
from algorithms.hyperneat_classes.genome_handler import GenomeType, GenomeHandler
from algorithms.hyperneat_classes.substrate import Substrate
from utils.dqn_reporter import evaluate_policy as evaluate_dqn
from utils.experiment_config import ExperimentConfig
from utils.hyperneat_reporter import evaluate_policy as evaluate_hyperneat
from utils.matrix_op import concatenate_per_row
from utils.plot_ut import *


class HyperNeatDqnLoop(JoinedHyperneatDqn):
    """
    Looped combination of HyperNEAT and DQN. DQN model is additionally backpropagated to HyperNEAT population.

    :param main_config: Experiment config
    :param code_dir: Directory where the results are saved
    :param experiment_name: Name of the experiment, serves as identifier to save the results
    :param run_id: Id of the run to distinguish between different runs with the same hyper parameters
    :param max_reward: Reward where the environment is considered to be solved
    """

    def __init__(self, main_config: ExperimentConfig, code_dir: Path, experiment_name: str, run_id: int,
                 max_reward: int):
        super().__init__(main_config, code_dir, experiment_name, run_id, max_reward)

    @staticmethod
    def get_all_possible_input_coo_for_weights(substrate, state_dict) -> Tuple[np.ndarray, np.ndarray]:
        """
        Get all possible combinations of neuron coordinates and the true weights.
        :param substrate: neural network substrate
        :param state_dict: state dict of parameters from the policy trained by DQN
        :return: dataset: x (coordinates) and y (weights, biases)
        """
        # List[(layer_id_1, nodes_id_1, layer_id_2, nodes_id_2)]

        expected_output_from_dqn = []
        layer_list = [(substrate.num_inputs, substrate.num_nodes_in_layers)] + \
                     (substrate.num_layers - 1) * [(substrate.num_nodes_in_layers, substrate.num_nodes_in_layers)] + \
                     [(substrate.num_nodes_in_layers, substrate.num_outputs)]
        all_cppn_inputs = []
        for l1_row, (l1_num_cols, l2_num_cols) in enumerate(layer_list):
            l2_row = l1_row + 1
            coords_l0 = np.stack((
                (l1_row / float(substrate.num_layers + 1)) * np.ones(l1_num_cols),  # layer_id
                np.arange(l1_num_cols) / float(l1_num_cols - 1)  # nodes
            ), axis=1)
            coords_l1 = np.stack((
                (l2_row / float(substrate.num_layers + 1)) * np.ones(l2_num_cols),
                np.arange(l2_num_cols) / float(l2_num_cols - 1)
            ), axis=1)
            cppn_input = concatenate_per_row(coords_l0, coords_l1)
            all_cppn_inputs += [cppn_input]
            layer_key = f'q_net.q_net.{l1_row * 2}.weight'
            y = state_dict[layer_key]
            y = y.view(-1, 1).cpu().numpy()
            expected_output_from_dqn += [y]
        all_cppn_inputs = np.concatenate(all_cppn_inputs)
        expected_output_from_dqn = np.concatenate(expected_output_from_dqn)
        return all_cppn_inputs, expected_output_from_dqn

    @staticmethod
    def get_all_possible_input_coo_for_biases(substrate, state_dict) -> Tuple[np.ndarray, np.ndarray]:
        """
        Get all neuron coordinates and the true biases.
        :param substrate: neural network substrate
        :param state_dict: state dict of parameters from the policy trained by DQN
        :return: dataset: x (coordinates) and y (weights, biases)
        """
        # List[(layer_id_1, nodes_id_1, layer_id_2, nodes_id_2)]
        expected_output_from_dqn = []
        layer_list = [(substrate.num_inputs, substrate.num_nodes_in_layers)] + \
                     (substrate.num_layers - 1) * [(substrate.num_nodes_in_layers, substrate.num_nodes_in_layers)] + \
                     [(substrate.num_nodes_in_layers, substrate.num_outputs)]
        all_cppn_inputs = []
        for l1_row, (_, l2_num_cols) in enumerate(layer_list):
            l2_row = l1_row + 1
            layer_id = (l2_row / float(substrate.num_layers + 1)) * np.ones(l2_num_cols)
            nodes_id = np.arange(l2_num_cols) / float(l2_num_cols - 1)
            cppn_input = np.stack((layer_id, nodes_id, layer_id, nodes_id), axis=1)
            all_cppn_inputs += [cppn_input]
            layer_key = f'q_net.q_net.{l1_row * 2}.bias'
            y = state_dict[layer_key]
            y = y.view(-1, 1).cpu().numpy()
            expected_output_from_dqn += [y]
            print(f'{layer_key=} {cppn_input.shape=} {y.shape=}')
        all_cppn_inputs = np.concatenate(all_cppn_inputs)
        expected_output_from_dqn = np.concatenate(expected_output_from_dqn)
        return all_cppn_inputs, expected_output_from_dqn

    @staticmethod
    def select_x_based_on_genome_type(genome_handler: GenomeHandler, subgenome_key, coordinates_biases,
                                      coordinates_weights):
        """
        Return dataset based on genome type (weights or biases; or their combination)

        :param genome_handler: CPPN selector
        :param subgenome_key: CPPN key
        :param coordinates_biases: Bias coordinates
        :param coordinates_weights: Weight coordinates
        :return: the selected dataset
        """
        # return bias only, weights only, both concat
        assert genome_handler.use_bias, "Genomes without bias not supported"

        if genome_handler.num_cppn_subgenomes == 2:
            if genome_handler.subgenomes_names[subgenome_key] == 'weight':
                return coordinates_weights
            elif genome_handler.subgenomes_names[subgenome_key] == 'bias':
                return coordinates_biases
            assert f"Subgenome key must stand for bias or weight, found {genome_handler.subgenomes_names[subgenome_key]}"
        else:
            return np.concatenate([coordinates_weights, coordinates_biases])

    @staticmethod
    def select_y_based_on_genome_type(genome_handler, subgenome_key, expected_biases, expected_weights):
        """
        Return dataset based on genome type (weights or biases; or their combination)

        :param genome_handler: CPPN selector
        :param subgenome_key: CPPN key
        :param expected_biases: Bias coordinates
        :param expected_weights: Weight coordinates
        :return: the selected dataset
        """
        assert genome_handler.use_bias, "Genomes without bias not supported"

        if genome_handler.num_cppn_subgenomes == 2:
            if genome_handler.subgenomes_names[subgenome_key] == 'weight':
                return expected_weights
            elif genome_handler.subgenomes_names[subgenome_key] == 'bias':
                return expected_biases
            assert f"Subgenome key must stand for bias or weight, found {genome_handler.subgenomes_names[subgenome_key]}"
        else:
            return np.concatenate([expected_weights, expected_biases])

    @staticmethod
    def cppns_to_genome(cppns: Dict[int, CppnNetwork], genome: MultiGenome):
        """
        Copy back propagated data to the original genome
        :param cppns: cppns with backproped weights or biases
        :param genome: the original genome
        """
        for subgenome_key, subgenome_value in genome.subgenomes.items():
            cppn = cppns[subgenome_key]
            for node_id, node in subgenome_value.nodes.items():
                if node_id in cppn.node_to_str_bias:
                    new_bias = cppn.biases_n_key_to_tensor[cppn.node_to_str_bias[node_id]].item()
                    subgenome_value.nodes[node_id].bias = new_bias
            for (edge_from_id, edge_to_id), edge in filter(lambda kv_pair: kv_pair[1].enabled,
                                                           subgenome_value.connections.items()):
                new_weight = cppn.weights_e_key_to_tensor[cppn.edge_to_str_w[(edge_from_id, edge_to_id)]].item()
                subgenome_value.connections[(edge_from_id, edge_to_id)].weight = new_weight

    def backprop_dqn_model_to_genome(self, genome: MultiGenome, dqn_model: DQN, neat_cfg, exp_cfg, n_actions,
                                     n_observations, orig_fitness) -> MultiGenome:
        """
        Run SGD to backprop weights and biases to genome.
        :param genome: original genome
        :param dqn_model: dqn model
        :param neat_cfg: neat configuration
        :param exp_cfg: experiment configuration
        :param n_actions: number of environment actions
        :param n_observations: number of environment observations
        :param orig_fitness: original fitness of the genome
        :return: fine-tuned genome
        """
        # convert genome -> cppn
        cppns: Dict[int, CppnNetwork] = dict()
        for subgenome_key, subgenome_value in genome.subgenomes.items():
            cppns[subgenome_key] = CppnNetwork.create(genome=subgenome_value, config=neat_cfg.genome_config,
                                                      device=exp_cfg.device)
        cppns_copy = cppns.copy()
        substrate = Substrate(num_layers=exp_cfg.num_layers, num_nodes_in_layers=exp_cfg.num_nodes_per_hidden_layer,
                              num_inputs=n_observations, num_outputs=n_actions)
        dqn_model_state_dict = dqn_model.policy.state_dict()
        dataloaders = self.get_dataloaders(cppns, genome, substrate, dqn_model_state_dict)

        for epoch_id in range(self.exp_cfg.cppn_num_backprop_epochs):
            criterion = torch.nn.MSELoss()
            fitness_hyperneat2 = (-np.inf, -np.inf)
            for subgenome_key, cppn in cppns.items():
                optimizer = torch.optim.Adam(cppn.parameters(), lr=0.001)
                running_loss = 0
                for batch_coordinates, expected_weights in dataloaders[subgenome_key]:
                    optimizer.zero_grad()
                    real_weights = cppn.forward(x=batch_coordinates)
                    loss = criterion(real_weights, expected_weights)
                    loss.backward()
                    optimizer.step()
                    running_loss += loss.item()
                HyperNeatDqnLoop.cppns_to_genome(cppns_copy, genome)
                new_genome_net = self.alg1_hyperneat.make_net(genome)
                fitness_hyperneat2 = evaluate_hyperneat(new_genome_net, self.alg1_hyperneat.get_vec_env(
                    seed=self.alg1_hyperneat.exp_cfg.seed,
                    max_episode_steps=self.alg1_hyperneat.exp_cfg.max_episode_length),
                                                        self.alg1_hyperneat.exp_cfg.max_test_iterations,
                                                        self.alg1_hyperneat.exp_cfg.max_episode_length)
                print(
                    f'{subgenome_key} {epoch_id=} {fitness_hyperneat2} {running_loss / len(dataloaders[subgenome_key])}')
            if fitness_hyperneat2[0] / orig_fitness < self.exp_cfg.cppn_backprop_quality_threshold:
                break
        HyperNeatDqnLoop.cppns_to_genome(cppns, genome)
        return genome

    def get_dataloaders(self, cppns, genome, substrate, dqn_model_state_dict):
        """
        Get dataset and initialize data loaders.
        :param cppns: dictionary of CPPNs
        :param genome: original genome
        :param substrate: substrate of neural network
        :param dqn_model_state_dict: DQN policy state dict
        :return: the dataloader
        """
        dataloaders = {}
        coordinates_weights, expected_weights = HyperNeatDqnLoop.get_all_possible_input_coo_for_weights(substrate,
                                                                                                        dqn_model_state_dict)
        coordinates_biases, expected_biases = HyperNeatDqnLoop.get_all_possible_input_coo_for_biases(substrate,
                                                                                                     dqn_model_state_dict)
        for subgenome_key, cppn in cppns.items():
            coordinates = HyperNeatDqnLoop.select_x_based_on_genome_type(genome.genome_handler, subgenome_key,
                                                                         coordinates_biases, coordinates_weights)
            expected = HyperNeatDqnLoop.select_y_based_on_genome_type(genome.genome_handler, subgenome_key,
                                                                      expected_biases, expected_weights)
            coordinates = torch.tensor(coordinates, dtype=torch.float, device=self.exp_cfg.device, requires_grad=True)
            expected = torch.tensor(expected, dtype=torch.float, device=self.exp_cfg.device, requires_grad=True)
            dataset = TensorDataset(coordinates, expected)
            dataloader = DataLoader(dataset, batch_size=256, shuffle=True)
            dataloaders[subgenome_key] = dataloader
        return dataloaders

    def create_new_pop(self, population, new_genome):
        """
        Create new population by mutating the fine-tuned genome to the old population.
        :param population: the original population
        :param new_genome: the fine tuned genome
        :return:
        """
        min_fitness, worst_ind = np.inf, None
        self.alg1_hyperneat.eval_all_genomes(list(iteritems(population.population)), self.alg1_hyperneat.neat_cfg)
        new_population = {}

        for ind in population.population:
            gid = next(population.reproduction.genome_indexer)
            child = self.alg1_hyperneat.neat_cfg.genome_type(gid, self.alg1_hyperneat.neat_cfg.genome_config)
            child.configure_crossover(population.population[ind], new_genome,
                                      self.alg1_hyperneat.neat_cfg.genome_config)
            # child.mutate(self.alg1_hyperneat.neat_cfg.genome_config)
            new_population[gid] = child
            population.reproduction.ancestors[gid] = (ind, new_genome.key)
            fitness_hyperneat3, _ = evaluate_hyperneat(self.alg1_hyperneat.make_net(new_population[gid]),
                                                       self.alg1_hyperneat.get_vec_env(
                                                           seed=self.alg1_hyperneat.exp_cfg.seed,
                                                           max_episode_steps=self.alg1_hyperneat.exp_cfg.max_episode_length),
                                                       self.alg1_hyperneat.exp_cfg.max_test_iterations,
                                                       self.alg1_hyperneat.exp_cfg.max_episode_length)
            print(f"{population.population[ind].fitness} {fitness_hyperneat3}")
            child.set_fitness(fitness_hyperneat3)
            if fitness_hyperneat3 < min_fitness:
                min_fitness, worst_ind = fitness_hyperneat3, gid
        new_population[worst_ind] = new_genome
        population.population = new_population
        species = self.alg1_hyperneat.neat_cfg.species_set_type(self.alg1_hyperneat.neat_cfg.species_set_config,
                                                                self.alg1_hyperneat.pop.reporters)
        species.speciate(self.alg1_hyperneat.neat_cfg, population.population, population.generation)
        # population.species = species
        return population, species

    def train(self) -> Tuple[Tuple[MultiGenome, DQN], Tuple[Tuple[float, float], Tuple[float, float]]]:
        """
        Train the HyperNEAT, DQN loop, backprop the solution from DQN to HyperNEAT population.
        Iterate.
        :return: the best models after the several iterations
        """
        # Algorithms init
        self.alg1_hyperneat = HyperNEAT(self.exp_cfg, self.code_dir, self.experiment_name, self.run_id)
        self.alg1_hyperneat.neat_cfg.fitness_threshold = self.max_reward
        self.alg2_dqn = Dqn(self.exp_cfg, self.code_dir, self.experiment_name, self.run_id, self.max_reward)

        best_genome, fitness_hyperneat, fitness_dqn = None, -np.inf, -np.inf
        solution_found = False

        for iteration in range(self.exp_cfg.num_iterations_in_hn_dqn_loop):
            if solution_found:
                break
            print(f"Running iteration {iteration}")
            #############################  Train HyperNEAT + eval  ################################################
            self.alg1_hyperneat.train()
            best_genome = self.alg1_hyperneat.load_best_solution()
            final_net = self.alg1_hyperneat.make_net(best_genome)
            fitness_hyperneat = evaluate_hyperneat(final_net, env=self.get_vec_env(seed=self.exp_cfg.seed,
                                                                                   max_episode_steps=self.exp_cfg.max_episode_length),
                                                   n_eval_episodes=self.exp_cfg.max_test_iterations,
                                                   max_episode_length=self.exp_cfg.max_episode_length)

            ####################################################### set initial solution to DQN
            state_dict = self.load_dqn_like_state_dict_from_hyperneat_net(final_net,
                                                                          full_update=self.exp_cfg.initialize_both_q_nets)
            if self.exp_cfg.load_replay_buffer:
                self.create_and_save_replay_buffer(final_net)
            if self.exp_cfg.use_external_policy:
                self.alg2_dqn.model.external_policy = final_net
            self.alg2_dqn.load_solution(initial_solution=state_dict,
                                        force_full_update=self.exp_cfg.initialize_both_q_nets,
                                        replay_buffer_path=self.replay_buffer_path if self.exp_cfg.load_replay_buffer else None)

            #############################  Train DQN + eval  ################################################
            self.alg2_dqn.train()
            best_model = self.alg2_dqn.load_best_solution()
            fitness_dqn = evaluate_dqn(best_model, env=self.get_vec_env(seed=self.exp_cfg.seed,
                                                                        max_episode_steps=self.exp_cfg.max_episode_length),
                                       n_eval_episodes=self.exp_cfg.max_test_iterations,
                                       max_episode_length=self.exp_cfg.max_episode_length)
            self.plot_results(models=(best_genome, best_model), exp_id=self.exp_id, duration=0,
                              device=self.exp_cfg.device)
            self.save((best_genome, best_model))

            ####################################################### BACKPROP DQN to CPPN
            new_genome: MultiGenome = self.backprop_dqn_model_to_genome(genome=best_genome,
                                                                        dqn_model=self.alg2_dqn.model,
                                                                        neat_cfg=self.alg1_hyperneat.neat_cfg,
                                                                        exp_cfg=self.alg1_hyperneat.exp_cfg,
                                                                        n_actions=self.alg1_hyperneat.n_actions,
                                                                        n_observations=self.alg1_hyperneat.n_observations,
                                                                        orig_fitness=fitness_hyperneat[0])
            fitness_hyperneat2 = evaluate_hyperneat(self.alg1_hyperneat.make_net(new_genome),
                                                    self.alg1_hyperneat.get_vec_env(
                                                        seed=self.alg1_hyperneat.exp_cfg.seed,
                                                        max_episode_steps=self.alg1_hyperneat.exp_cfg.max_episode_length),
                                                    self.alg1_hyperneat.exp_cfg.max_test_iterations,
                                                    self.alg1_hyperneat.exp_cfg.max_episode_length)
            new_genome.set_fitness(fitness_hyperneat2[0])
            print(f'GENOME before vs after everything -> {fitness_hyperneat=} {fitness_dqn=} {fitness_hyperneat2=}')
            solution_found = fitness_hyperneat[0] > self.max_reward or fitness_dqn[0] > self.max_reward or \
                             fitness_hyperneat2[0] > self.max_reward

            ####################################################### INIT pop and save
            population, species = self.create_new_pop(self.alg1_hyperneat.pop, new_genome)
            self.alg1_hyperneat.save_backproped_result(new_genome)
            self.alg1_hyperneat.plot_genome(new_genome, backproped=True)

            ####################################################### INIT algs for next round
            exp_name = self.alg1_hyperneat.experiment_name[
                       :self.alg1_hyperneat.experiment_name.find("iter") + 4] + f"{iteration + 1}"
            print(f"New exp name: {exp_name}")

            self.exp_cfg.seed += 1
            self.alg1_hyperneat.pop = population
            self.alg1_hyperneat.pop.generation = 0
            self.alg1_hyperneat.pop.remove_reporter(self.alg1_hyperneat.logger)
            self.alg1_hyperneat.experiment_name = exp_name + "_HyperNEAT"
            self.alg1_hyperneat.set_paths()
            self.alg1_hyperneat.logger = Reporter(self.alg1_hyperneat.logger_path,
                                                  self.alg1_hyperneat.exp_cfg.num_valid_evaluations,
                                                  self.alg1_hyperneat.best_so_far_hyperneat_solution_path)

            self.alg2_dqn = Dqn(self.exp_cfg, self.code_dir, exp_name, self.run_id, self.max_reward)

        return (best_genome, self.alg2_dqn.model), (fitness_hyperneat, fitness_dqn)


if __name__ == '__main__':
    env_name, max_reward = ('Acrobot-v1', -60)
    code_dir = Path(__file__).resolve().parent.parent.parent
    config_path = code_dir.joinpath('configs', 'config.yml')
    SEEDS = [260679797, 701623387, 882840679, 157005113, 871591532, 609579736, 73563671, 312938638, 180649226,
             403804907]

    exp_id = 92
    for run_id in range(2, 3):
        experiment_config = ExperimentConfig.create(alg_name='HyperNEAT_DQN_loop', cfg_path=config_path,
                                                    env_name=env_name, seed=SEEDS[run_id], device='cuda',
                                                    gen_type=GenomeType.W_B_2, gamma=0.9,
                                                    target_update_frequency=10000,
                                                    replay_buffer_epsilon=0.2, init_q_net=False)
        experiment_config.prefilled_buffer_size = 1000
        experiment_config.use_external_policy = True
        experiment_config.external_policy_threshold_time_steps = 200000
        experiment_config.cppn_backprop_quality_threshold = 0.9
        experiment_config.cppn_num_backprop_epochs = 20
        experiment_config.max_train_iterations = 200
        experiment_config.num_iterations_in_hn_dqn_loop = 10
        experiment_config.nn_hidden_activation = 'leakyrelu'

        start = time()

        print(f'Running experiment {env_name=}')
        exp_name = f'exp{exp_id:03}_{experiment_config.algorithm_name}_{env_name}_run{run_id}_iter{0}'

        algorithm = HyperNeatDqnLoop(experiment_config, code_dir, exp_name, run_id, max_reward)

        (best_genome, final_model), best_fitness = algorithm.train()
        print(
            f"\n\n\n--------------------\nFinal performance: {best_fitness}")
        run_duration = time() - start
        print(
            f'Experiment {exp_name} run for {run_duration} s which is {run_duration / 60} min {run_duration % 60} s')
