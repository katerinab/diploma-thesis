from collections import defaultdict
from enum import Enum
from pathlib import Path

import matplotlib.pyplot as plt
from utils.hyperneat_reporter import Stats
from utils.plot_ut import plot_all_stats
import numpy as np


LOGGING_FREQUENCY = 500
class EvalType(Enum):
	PRINT_STATS = 0
	PLOT_MED = 1
	PLOT_95 = 2

stats_type = EvalType.PLOT_MED
folder_names = {
	'HN': 'results_600',
	'DQN': 'results_610',
	'HN->DQN (3)': 'results_670',
	'HN->DQN (2, 3)': 'results_660',
	'HN->DQN loop': 'results_680'
}


dp_dir = Path(__file__).resolve().parent.parent.joinpath("results_final")
env_names = ['CartPole', 'Acrobot', 'MountainCar']
min_reward = {
	'CartPole' : 0,
	'Acrobot': -200,
	'MountainCar': -200
}

def process_all_runs(exp_features, filenames, exp_id, ax=None):
	min_reward = [0, -200, -200]
	max_reward = [195, -60, -110]
	lims = [(0, 200), (-200, -60), (-200, -100)]
	stats_dict = dict()
	filenames = sorted(filenames)
	for f_id, f in enumerate(filenames):
		stats = Stats.load(f)
		stats_dict[f'run_{f_id}'] = stats
	rewards = np.array([stats.generation_stats[-1].fitness_best for stats in stats_dict.values()])
	timesteps = np.abs(np.array([stats.generation_stats[-1].time_steps for stats in stats_dict.values()]))
	total_steps = 8000000
	if stats_type == EvalType.PRINT_STATS:
		num_finished = np.array(
			[s.generation_stats[-1].fitness_best >= max_reward[exp_id % 5] for s in stats_dict.values()]).sum()
		mean_steps = int(timesteps.mean().round(decimals=2))
		median_steps = int(np.median(timesteps).round(decimals=2))
		std_steps = int(timesteps.std().round(decimals=2))
		mean_reward = int(rewards.mean().round(decimals=2))
		max_reward = int(rewards.max().round(decimals=2))
		min_reward = int(rewards.min().round(decimals=2))
		median_reward = int(np.median(rewards).round(decimals=2))
		std_reward = int(rewards.std().round(decimals=2))

		print(f'& {num_finished} & '
		  f'{mean_steps} & {np.round(100 * (mean_steps / total_steps), decimals=2)} & '
		  f'{std_steps} & {median_steps} & {mean_reward} & {std_reward} & {median_reward} & {max_reward} & {min_reward} \\\\')
	elif stats_type == EvalType.PLOT_MED:
		if env_names[0] in exp_features:
			med_id = np.where(timesteps == np.percentile(timesteps, 50, interpolation='nearest'))[0][0]
		else:
			med_id = np.where(rewards == np.percentile(rewards, 50, interpolation='nearest'))[0][0]
		print(f"{rewards=}\nMedian {rewards[med_id]} found in {med_id} {filenames[med_id]}")
		fb, fs = 18, 16
		ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
		ax.tick_params(axis='both', which='major', labelsize=fs)
		ax.tick_params(axis='both', which='minor', labelsize=fs)
		ax.xaxis.get_offset_text().set_fontsize(fs)
		x_vals = [s.time_steps for s in stats_dict[f'run_{med_id}'].generation_stats]
		best_fitness = [s.fitness_best for s in stats_dict[f'run_{med_id}'].generation_stats]
		# avg_fitness = [s.fitness_avg for s in stats_dict[f'run_{med_id}'].generation_stats]
		# plt.plot(x_vals, avg_fitness, 'b-', label="current")
		plt.plot(x_vals, best_fitness, '-.', label=f"{alg_name}")
		plt.xlabel("Time step", fontsize=fb)
		plt.ylabel("Fitness", fontsize=fb)
		plt.ylim(lims[exp_id % 5])
		plt.grid()
		plt.legend(loc="best", fontsize=fs)
		plt.suptitle(env_name + ' - median run', fontsize=fb)
	elif stats_type == EvalType.PLOT_95:
		all_stats = list(stats_dict.values())
		x_limit = max([stats.generation_stats[-1].time_steps for stats in all_stats])
		logged_time_steps = np.arange(start=0, stop=x_limit+1, step=LOGGING_FREQUENCY)
		logged_rewards = min_reward[exp_id % 5] * np.ones_like(logged_time_steps)
		cur_rewards = [min_reward[exp_id % 5] for _ in all_stats]
		stop_id = None
		for l_id, l_step in enumerate(logged_time_steps.tolist()):
			# print(l_step)
			all_empty = True
			for s_id in range(len(all_stats)):
				if len(all_stats[s_id].generation_stats) == 0:
					continue
				all_empty = False
				ts, bf = all_stats[s_id].generation_stats[0].time_steps, all_stats[s_id].generation_stats[0].fitness_best
				while ts <= l_step:
					all_stats[s_id].generation_stats.pop(0)
					cur_rewards[s_id] = bf
					if len(all_stats[s_id].generation_stats) == 0:
						break
					ts, bf = all_stats[s_id].generation_stats[0].time_steps, all_stats[s_id].generation_stats[0].fitness_best
					# print(f"{s_id=} {l_step=} | {ts=} {bf=}")
			r_in_l_step_with_q = np.quantile(cur_rewards, QUANTILE, interpolation='lower')
			logged_rewards[l_id] = r_in_l_step_with_q
			if all_empty:
				stop_id = l_id
				break
		if stop_id is not None:
			logged_rewards = logged_rewards[:stop_id+1]
			logged_time_steps = logged_time_steps[:stop_id+1]

		fb, fs = 18, 16
		# ax.set_xscale('log')
		ax.ticklabel_format(axis='x', style='sci', scilimits=(0, 0))
		ax.tick_params(axis='both', which='major', labelsize=fs)
		ax.tick_params(axis='both', which='minor', labelsize=fs)
		ax.xaxis.get_offset_text().set_fontsize(fs)
		plt.plot(logged_time_steps, logged_rewards, '-.', label=f"{alg_name}")
		plt.xlabel("Time step", fontsize=fb)
		plt.ylabel("Fitness", fontsize=fb)
		plt.ylim(lims[exp_id % 5])
		plt.grid()
		plt.legend(loc="best", fontsize=fs)
		plt.suptitle(env_name + ', p=' + str(QUANTILE), fontsize=fb)


if __name__ == '__main__':
	print('env & alg & done & $T_{avg}$ & $\\frac{T_{avg}}{T_{max}}\%$ & $T_{std}$ & $T_{med}$ & $R_{avg}$ & $R_{std}$ & $R_{med}$ & $R_{max}$ & $R_{min}$ \\\\ \hline \hline')
	print("ENVIRONMENT &&&&&&&&&&\\\\")
	for QUANTILE in [0.99]:
		for env_id in range(0, 3):
			ax = None
			if stats_type == EvalType.PLOT_MED or stats_type == EvalType.PLOT_95:
				fig = plt.figure()
				ax = plt.subplot()
			env_name = env_names[env_id]
			for alg_name, folder_name in folder_names.items():
				exp_id = int(folder_name.split("_")[-1]) + env_id
				if exp_id == 680:
					continue
				if exp_id >= 680 and exp_id <= 682:
					exp_id += 5
				stats_path = dp_dir.joinpath('logs_merged', f'exp{exp_id}')
				fig_path = dp_dir.joinpath('fig_merged_inter', f'exp{exp_id}')
				files = [x for x in stats_path.iterdir() if (x.is_file() and x.name.endswith('stats.yml'))]

				fig_path.mkdir(parents=True, exist_ok=True)
				if stats_type == EvalType.PRINT_STATS:
					print(f'{exp_id} {env_name} {alg_name}', end=' ')
				else:
					print(f'-------\n{exp_id} {env_name} {alg_name}', end='\n')
				process_all_runs(f"{env_name} {alg_name}", files, exp_id, ax)

			if stats_type == EvalType.PLOT_MED:
				fig_path = dp_dir.joinpath(f'{env_name}_stats.pdf')
				plt.tight_layout()
				plt.savefig(fig_path)
				plt.show()
				plt.close()

			if stats_type == EvalType.PLOT_95:
				fig_path = dp_dir.joinpath(f'{env_name}_{QUANTILE}_stats.pdf')
				plt.tight_layout()
				plt.savefig(fig_path)
				plt.show()
				plt.close()
