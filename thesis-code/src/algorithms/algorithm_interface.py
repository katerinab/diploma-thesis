import random
from pathlib import Path
from typing import Optional, Tuple

import gym
import numpy as np
import torch
from gym.wrappers import TimeLimit
from stable_baselines3 import DQN
from stable_baselines3.common.vec_env import DummyVecEnv

from algorithms.neat_extensions.multigenome import MultiGenome
from utils.experiment_config import ExperimentConfig
from utils.io import create_dir_and_parents_if_not_exist


class AlgorithmInterface:
    """
    Interface for the algorithms implemented in the project.
    Sets seeds, creates the files.

    :param experiment_cfg: Experiment config
    :param code_dir: Directory where the results are saved
    :param exp_name: Name of the experiment, serves as identifier to save the results
    :param run_id: Id of the run to distinguish between different runs with the same hyper parameters
    """

    def __init__(self, experiment_cfg: ExperimentConfig, code_dir: Path, exp_name: str, run_id: int):
        self.exp_cfg = experiment_cfg
        self.code_dir = code_dir
        self.experiment_name = exp_name
        self.exp_id = self.experiment_name.split("_")[0]
        self.run_id = run_id
        self.resources_path = self.code_dir.joinpath('resources')
        self.configs_path = self.code_dir.joinpath('configs')
        self.figures_path = self.resources_path.joinpath('fig', self.exp_id)
        self.logs_path = self.resources_path.joinpath('logs', self.exp_id)
        self.runs_path = self.resources_path.joinpath('runs')
        create_dir_and_parents_if_not_exist(self.runs_path)
        create_dir_and_parents_if_not_exist(self.logs_path)
        create_dir_and_parents_if_not_exist(self.figures_path)
        # load paths
        self.set_paths()
        self.set_seeds()

    def set_paths(self):
        """
        Initialize all paths
        """
        self.neat_config_path = self.configs_path.joinpath('hyperneat_neat_part.cfg')
        self.logger_path = self.logs_path.joinpath(f'{self.experiment_name}_stats.yml')
        self.replay_buffer_path = self.logs_path.joinpath(f'{self.experiment_name}_replay_buffer.pickle')

        assert not self.logger_path.exists(), f"File {self.logger_path} already exists, choose different exp id or run id"
        self.solution_path = self.logs_path.joinpath(f'{self.experiment_name}_solution.pickle')
        self.backproped_solution_path = self.logs_path.joinpath(f'{self.experiment_name}_midsolution_backproped.pickle')
        self.popsolution_path = self.logs_path.joinpath(f'{self.experiment_name}_population.pickle')
        self.popsolution_backproped_path = self.logs_path.joinpath(
            f'{self.experiment_name}_population_backproped.pickle')
        self.best_so_far_dqn_solution_path = self.logs_path.joinpath(f'{self.experiment_name}_solution_bsf_dqn.pickle')
        self.best_so_far_hyperneat_solution_path = self.logs_path.joinpath(
            f'{self.experiment_name}_solution_bsf_hyperneat.pickle')
        self.backproped_solution_path = self.logs_path.joinpath(f'{self.experiment_name}_midsolution_backproped.pickle')
        self.popsolution_path = self.logs_path.joinpath(f'{self.experiment_name}_population.pickle')
        self.popsolution_backproped_path = self.logs_path.joinpath(
            f'{self.experiment_name}_population_backproped.pickle')
        self.midsolution_path = self.logs_path.joinpath(f'{self.experiment_name}_midsolution.pickle')
        self.tensorboard_path = self.runs_path.joinpath(Path(self.exp_id, self.experiment_name))
        self.single_stats_path = self.figures_path.joinpath(self.experiment_name + '_stats.png')
        self.species_path = self.figures_path.joinpath(self.experiment_name + '_species.png')
        # save paths
        self.save_neat_config_path = self.logs_path.joinpath(f'{self.experiment_name}_neat_config.cfg')
        self.save_experiment_path = self.logs_path.joinpath(f'{self.experiment_name}_config.yml')

    def plot_results(self, best_result, exp_id, duration, device):
        pass

    def train(self) -> Tuple[Tuple[Optional[MultiGenome], Optional[DQN]], float]:
        pass

    def save(self, best_result):
        self.save_configs()
        self.save_final_result(best_result)

    def save_configs(self):
        pass

    def save_final_result(self, best_result):
        pass

    def set_seeds(self):
        """
        Set seeds to all used libraries
        """
        np.random.seed(self.exp_cfg.seed)
        torch.random.manual_seed(self.exp_cfg.seed)
        random.seed(self.exp_cfg.seed)

    def make_env(self) -> gym.Env:
        """
        Return the created gym environment
        :return: the created environment
        """
        env = gym.make(self.exp_cfg.environment)
        env.seed(random.randint(0, 2147483647))
        env.reset()
        return env

    def get_vec_env(self, max_episode_steps: int, seed: Optional[int] = None) -> DummyVecEnv:
        """
        Return the created gym environment in vectorized form
        :param max_episode_steps: maximal number of episode steps
        :param seed: seed to set to the environment
        :return: the created environment
        """
        env = DummyVecEnv([lambda: TimeLimit(gym.make(self.exp_cfg.environment), max_episode_steps=max_episode_steps)])
        if seed is None:
            seed = random.randint(0, 2147483647)
        env.seed(seed)
        env.reset()
        return env
