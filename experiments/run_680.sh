#!/bin/bash
#SBATCH --job-name=exp
#SBATCH --cpus-per-task=5
#SBATCH --mem=30G
#SBATCH --output=/home/brejcka1/final_loop%a_job%A.out
#SBATCH --error=/home/brejcka1/final_loop%a_job%A.err
#SBATCH --time=0-20:00:00
#SBATCH --mail-user=brejcka1@fel.cvut.cz
#SBATCH --mail-type=ALL
#SBATCH --exclude=node-12
#SBATCH --gres=gpu:1
#SBATCH --partition=gpu

stderr() {
    echo "$@" 1>&2; 
}

conda_init() {
    if conda info -e | grep $CONDA_ENV_NAME; then
        stderr "conda environment $CONDA_ENV_NAME already exists"
    else
        stderr "creating conda environment $CONDA_ENV_NAME..."
        conda env create --name $CONDA_ENV_NAME --file=/home/brejcka1/diploma-thesis/thesis-code/configs/environment.yml &
    fi
}


echo "QUERIED RESOURCES: SBATCH_GRES: $SBATCH_GRES | $SBATCH_PARTITION"

echo "--------- JOB INFO -------------------"
echo "SLURM_JOB_NAME: $SLURM_JOB_NAME"
echo "SLURM_JOB_PARTITION: $SLURM_JOB_PARTITION"
echo "SLURMD_NODENAME:: $SLURMD_NODENAME:"
echo "SLURM_TASK_PID: $SLURM_TASK_PID"
echo "SLURM_JOB_ID: $SLURM_JOB_ID"
echo "SLURM_GTIDS: $SLURM_GTIDS"
echo "SLURM_ARRAY_TASK_ID: $SLURM_ARRAY_TASK_ID"
echo "--------------------------------------"
            
ENVS=(CartPole-v0 Acrobot-v1 MountainCar-v0)
SEEDS=(1786801995 865889505 635211799 813965581 928817231 988660161 1413391619 1415962418 939759407 2006424714 662742212 157719621 188699706 256554496 1859664984 1528972715 1327032207 1113210313 1705844787 679643424 1204126539 1685191192 1306892393 1185676491 351168033 319696268 1396211524 1868124291 374429603 1185548011 1529468685 729438544 1422699604 2031977130 503831813)
METHODS=(HyperNEAT DQN Joined_HyperNEAT_DQN HyperNEAT_DQN_loop)


EXP_BASE=680
METHOD_ID=3

echo $'\n...READING EXPERIMENT FLAGS'
EXP_ID=$((EXP_BASE+ENV_ID))
ENV_ID=$(((SLURM_ARRAY_TASK_ID%2) + 1))
RUN_ID=$((SLURM_ARRAY_TASK_ID/2))

echo "FLAGS: $EXP_ID $METHOD_ID $ENV_ID $TRAIN_ITER_ID $B_QUALITY_ID"


echo $'\n...LOADING MODULES AND CONDA INIT'
module load Anaconda3/2020.07
. /opt/apps/software/Anaconda3/2020.07/etc/profile.d/conda.sh

TORCH_DEVICE=${SLURM_JOB_PARTITION//compute/cpu}
TORCH_DEVICE=${TORCH_DEVICE//gpu/cuda}
NODE_PARTIAL_NAME=${SLURMD_NODENAME//-[0-9]*/}
CONDA_ENV_NAME='dp-env-'$TORCH_DEVICE'-'$NODE_PARTIAL_NAME
echo "CONDA_ENV_NAME: $CONDA_ENV_NAME"
conda_init
conda activate $CONDA_ENV_NAME
conda env list

python --version
cd /home/brejcka1/diploma-thesis/thesis-code/src/

echo $'\n...INIT DONE, RUNNING THE PYTHON SCRIPT'
PYTHONPATH=.:../../external_libraries:$PYTHONPATH


echo "Running experiment with settings: $EXP_ID $RUN_ID $ENV_ID $GENOME_ID $TORCH_DEVICE"
OUTPUT='/home/brejcka1/exp_'$EXP_ID'_method'$METHOD_ID'_run'$RUN_ID'_'$TRAIN_ITER_ID'_'$SLURM_JOB_ID'_'$SLURM_ARRAY_TASK_ID'.out'
ERROR='/home/brejcka1/exp_'$EXP_ID'_method'$METHOD_ID'_run'$RUN_ID'_'$TRAIN_ITER_ID'_'$SLURM_JOB_ID'_'$SLURM_ARRAY_TASK_ID'.err'
echo "Printing OUTPUT to $OUTPUT"
python experiment.py \
  --algorithm_name ${METHODS[$METHOD_ID]} \
  --exp_id $EXP_ID \
  --run_id $RUN_ID \
  --environment ${ENVS[$ENV_ID]} \
  --seed ${SEEDS[$RUN_ID]} \
  --max_train_iterations 200 \
  --num_valid_evaluations 10 \
  --max_test_iterations 10 \
  --max_episode_length 200 \
  --device $TORCH_DEVICE \
  --num_layers 5 \
  --num_nodes_per_hidden_layer 32 \
  --nn_out_activation identity \
  --nn_hidden_activation leakyrelu \
  --n_processes 1 \
  --genome_type CPPN\(weights,bias\)_2out \
  --pop_size 20 \
  --dqn_num_rollouts 20 \
  --dqn_batch_size 64 \
  --gamma 0.9 \
  --target_update_frequency 10000 \
  --initialize_both_q_nets false \
  --load_replay_buffer false \
  --prefilled_buffer_size 50000 \
  --default_learning_starts 50000  \
  --replay_buffer_epsilon 0.2 \
  --init_q_net false \
  --use_external_policy true \
  --external_policy_threshold_time_steps 100000 \
  --cppn_backprop_quality_threshold 0.01 \
  --cppn_num_backprop_epochs 20 \
  --num_iterations_in_hn_dqn_loop 10 \
2> $ERROR 1> $OUTPUT

echo $'\n...PROCESS FINISHED'

exit 0
