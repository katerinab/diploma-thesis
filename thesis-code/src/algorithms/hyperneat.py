import multiprocessing
import pickle
from time import time

import torch
from stable_baselines3 import DQN
from torch.utils.tensorboard import SummaryWriter

from algorithms.algorithm_interface import AlgorithmInterface
from algorithms.neat_extensions.multigenome import MultiGenome
from algorithms.neat_extensions.multireproduction import MultiReproduction
from algorithms.hyperneat_classes.activations import str_to_activation_layer
from algorithms.hyperneat_classes.cppn_network import CppnNetwork
from algorithms.hyperneat_classes.feed_forward_network import FeedForwardNetwork
from algorithms.hyperneat_classes.genome_handler import GenomeType
from algorithms.hyperneat_classes.substrate import Substrate
from utils.experiment_config import ExperimentConfig
from utils.plot_ut import *


class HyperNEAT(AlgorithmInterface):
    """
    PyTorch based HyperNEAT implementation

    partially inspired by https://github.com/uber-research/PyTorch-NEAT/

    :param main_config: Experiment config
    :param code_dir: Directory where the results are saved
    :param experiment_name: Name of the experiment, serves as identifier to save the results
    :param run_id: Id of the run to distinguish between different runs with the same hyper parameters
    """

    def __init__(self, main_config: ExperimentConfig, code_dir: Path, experiment_name: str, run_id: int):
        super().__init__(experiment_cfg=main_config, code_dir=code_dir, exp_name=experiment_name + "_HyperNEAT",
                         run_id=run_id)
        self.neat_cfg = neat.Config(
            MultiGenome, MultiReproduction, neat.DefaultSpeciesSet, neat.DefaultStagnation, self.neat_config_path,
        )
        self.neat_cfg.genome_config.set_cppn_type(self.exp_cfg.genome_type)
        self.exp_cfg = main_config

        # load environment details
        env = self.make_env()
        self.n_actions = env.action_space.n
        self.n_observations = env.observation_space.shape[0]
        self.n_generations = self.exp_cfg.max_train_iterations / self.exp_cfg.pop_size
        assert self.exp_cfg.pop_size == self.neat_cfg.pop_size, "Population size must be the same as defined in NEAT config"
        # stats + logger
        self.pop = neat.Population(self.neat_cfg)
        self.logger = Reporter(self.logger_path, self.exp_cfg.num_valid_evaluations,
                               self.best_so_far_hyperneat_solution_path)

    def save_configs(self):
        self.exp_cfg.save(self.save_experiment_path)
        self.neat_cfg.save(self.save_neat_config_path)

    def save_final_result(self, best_results):
        """
        Save final result and population.
        :param best_results: final genome
        """
        best_result = best_results[0]
        with open(self.solution_path, "wb") as f:
            pickle.dump(best_result, f)
        with open(self.popsolution_path, "wb") as f:
            pickle.dump(self.pop, f)

    def save_backproped_result(self, best_result):
        """
        Save best performing result and corresponding population.
        :param best_result: final genome
        """
        with open(self.backproped_solution_path, "wb") as f:
            pickle.dump(best_result, f)
        with open(self.popsolution_backproped_path, "wb") as f:
            pickle.dump(self.pop, f)

    def load_best_solution(self):
        with open(self.best_so_far_hyperneat_solution_path, "rb") as f:
            best_result = pickle.load(f)
        return best_result

    def make_net(self, genome: MultiGenome) -> FeedForwardNetwork:
        """
        Creates neural network given genome and substrate.
        :param genome: Genome from HyperNEAT
        :return: the underlying neural network
        """
        # convert genome -> cppn
        cppns: Dict[int, CppnNetwork] = dict()
        for subgenome_key, subgenome_value in genome.subgenomes.items():
            cppns[subgenome_key] = CppnNetwork.create(genome=subgenome_value,
                                                      config=self.neat_cfg.genome_config,
                                                      device=self.exp_cfg.device,
                                                      )
        substrate = Substrate(num_layers=self.exp_cfg.num_layers,
                              num_nodes_in_layers=self.exp_cfg.num_nodes_per_hidden_layer,
                              num_inputs=self.n_observations,
                              num_outputs=self.n_actions)
        # convert cppn -> nn
        return FeedForwardNetwork.create(
            cppns,
            substrate,
            genome.genome_handler.weight_bias_selector,
            nn_out_activation=str_to_activation_layer[self.exp_cfg.nn_out_activation],
            nn_hidden_activation=str_to_activation_layer[self.exp_cfg.nn_hidden_activation],
            device=self.exp_cfg.device,
            use_bias=genome.genome_handler.use_bias
        )

    def activate_net(self, net: FeedForwardNetwork, states: np.ndarray) -> np.ndarray:
        """
        Call forward on the underlying network, and get argmax action.
        :param net: neural network
        :param states: input
        """
        states = torch.tensor(states, dtype=torch.float, device=self.exp_cfg.device, requires_grad=True)
        outputs = net.forward(states)
        return outputs.argmax(dim=1).cpu().numpy()

    def train(self) -> Tuple[Tuple[MultiGenome, Optional[DQN]], float]:
        """
        Train the population of individuals by HyperNEAT to obtain the best genome.
        :return:
        """
        self.pop.add_reporter(self.logger)
        winner = self.pop.run(self.eval_all_genomes, self.n_generations)
        self.set_fitness_to_genome(-1, winner)

        return (winner, None), winner.fitness,

    def eval_all_genomes(self, genomes, _):
        """
        Evaluate all genomes in the population
        :param genomes: list of genomes
        :param _: disabled parameter
        """
        if self.exp_cfg.n_processes > 1:
            with multiprocessing.Pool(processes=self.exp_cfg.n_processes) as pool:
                fitnesses = pool.starmap_async(self.set_fitness_to_genome, genomes).get()
            for (_, genome), fitness in zip(genomes, fitnesses):
                genome.set_fitness(fitness)
        else:
            for (genome_id, genome) in genomes:
                self.set_fitness_to_genome(genome_id, genome)

    def set_fitness_to_genome(self, _, genome: MultiGenome):
        """
        Evaluate one genome, get the mean of several batches.
        :param _: disabled parameter
        :param genome: genome representing the individual
        :return:
        """
        envs = [self.make_env() for _ in range(self.exp_cfg.num_valid_evaluations)]
        net = self.make_net(genome)
        batch_fitness = np.zeros(self.exp_cfg.num_valid_evaluations)
        batch_states = np.array([env.reset() for env in envs])
        batch_dones = [False] * self.exp_cfg.num_valid_evaluations

        step_num = 0
        while True:
            if all(batch_dones) or (
                    self.exp_cfg.max_episode_length is not None and step_num == self.exp_cfg.max_episode_length):
                break
            actions = self.activate_net(net, batch_states)
            assert len(actions) == len(envs)
            for i, (env, action, done) in enumerate(zip(envs, actions, batch_dones)):
                if not done:
                    state, reward, done, _ = env.step(action)
                    batch_fitness[i] += reward
                    if not done:
                        batch_states[i] = state
                    batch_dones[i] = done
            step_num += 1
        fitness = batch_fitness.mean()
        genome.set_fitness(fitness)
        return fitness

    def plot_genome(self, best_genome, backproped=False):
        """
        Plot graph representation of all CPPNs of the given genome.

        :param best_genome: genome to plot
        :param backproped: says whether the genome was fine-tuned by backpropagation or not
        """
        for subgenome_key, subgenome in enumerate(best_genome.subgenomes.values()):
            backproped_name = "_backproped" if backproped else ""
            cppn_name = 'CPPN_' + best_genome.genome_handler.get_cppn_name(subgenome_key) + backproped_name
            draw_net(config=self.neat_cfg, genome=subgenome,
                     path=self.figures_path.joinpath(self.experiment_name + f'_{cppn_name}'),
                     format='png', title=cppn_name.replace("_", " ") + " " + self.experiment_name)

    def plot_results(self, models, exp_id, duration, device):
        """
        Plot the resulting genome, population species, training performance, tensorboard log
        :param models: genome
        :param exp_id: experiment id
        :param duration: experiment duration
        :param device: device for tensors
        """
        best_genome = models[0]
        plot_stats(self.logger, self.single_stats_path,
                   subtitle=self.experiment_name.replace("_", " ") + f"\n{duration=:.2f} s")
        plot_species(self.logger, self.species_path)
        self.plot_genome(best_genome)
        final_net = self.make_net(best_genome)
        tb = SummaryWriter(self.tensorboard_path)
        tb.add_graph(final_net, torch.ones(1, self.n_observations, device=device))
        for i, layer in enumerate(final_net.layers):
            if isinstance(layer, torch.nn.Linear):
                if layer.bias is not None:
                    tb.add_histogram(f'Linear{i}.bias', layer.bias)
                tb.add_histogram(f'Linear{i}.weight', layer.weight)
        tb.close()


if __name__ == '__main__':
    env_name, max_reward = ('Acrobot-v1', -60)
    code_dir = Path(__file__).resolve().parent.parent.parent
    config_path = code_dir.joinpath('configs', 'config.yml')
    seed = 123456
    exp_id, run_id = 1, 4

    experiment_config = ExperimentConfig.create(alg_name='HyperNEAT', cfg_path=config_path, env_name=env_name,
                                                seed=seed, device='cuda', gen_type=GenomeType.W_B_2, gamma=None,
                                                target_update_frequency=None, replay_buffer_epsilon=None,
                                                init_q_net=True)
    start = time()

    print(f'Running experiment {env_name=}')
    exp_name = f'exp{exp_id:03}_{experiment_config.algorithm_name}_{env_name}_run{run_id}'
    algorithm = HyperNEAT(experiment_config, code_dir, exp_name, run_id)

    (best_genome, _), best_fitness = algorithm.train()
    print(
        f"\n\n\n--------------------\nFinal performance: {best_fitness}")
    run_duration = time() - start
    algorithm.plot_results(models=(best_genome, _), exp_id=exp_id, duration=run_duration,
                           device=experiment_config.device)
    algorithm.save((_, best_genome))
    print(f'Experiment {exp_name} run for {run_duration} s which is {run_duration / 60} min {run_duration % 60} s')
