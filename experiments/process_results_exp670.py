from collections import defaultdict
from pathlib import Path

import matplotlib.pyplot as plt
from utils.hyperneat_reporter import Stats
from utils.plot_ut import plot_all_stats
import numpy as np

dp_dir = Path(__file__).resolve().parent.parent
exp_id = 670
folder_name = 'results_670'
max_reward = {670: 195, 671: -60, 672: -110}

def process_all_runs(exp_features, filenames, buffer_loaded=False):
	stats_dict = dict()
	filenames = sorted(filenames)
	for f_id, f in enumerate(filenames):
		stats = Stats.load(f)
		stats_dict[f'run_{f_id}'] = stats
	rewards = np.array([stats.generation_stats[-1].fitness_best for stats in stats_dict.values()])
	timesteps = np.abs(np.array([stats.generation_stats[-1].time_steps for stats in stats_dict.values()]))
	num_finished = np.array([1 if stats.generation_stats[-1].fitness_best >= max_reward[exp_id] else 0 for stats in stats_dict.values()]).sum()
	mean_steps = int(timesteps.mean().round())
	median_steps = int(np.median(timesteps).round())
	std_steps = int(timesteps.std().round())
	mean_reward = int(rewards.mean().round())
	median_reward = int(np.median(rewards).round())
	std_reward = int(rewards.std().round())
	total_steps = 4000000
	batch_size = 64
	rollout_size = 20
	init_steps = 0
	available_iterations = (total_steps - init_steps) / rollout_size
	mean_performed_iterations = (mean_steps - init_steps) / rollout_size
	mean_performed_grad_updates = mean_performed_iterations * batch_size

	print(f'{num_finished} {total_steps} {init_steps} {np.round(100 * (init_steps / total_steps))} '
		  f'{mean_steps} {np.round(100 * (mean_steps / total_steps))} {(mean_steps - init_steps)} {np.round(100 * ((mean_steps - init_steps) / total_steps))} '
		  f'{std_steps} {median_steps} {mean_reward} {std_reward} {median_reward} {available_iterations} {mean_performed_iterations} {mean_performed_grad_updates} ')
	features_name = "_".join(exp_features)
	fig_path = dp_dir.joinpath(folder_name, f'exp{exp_id:03}_{features_name}_stats.png')
	plot_all_stats(stats_dict, shape=(5, 6), pop_size=20, path=fig_path, env_name=f'{exp_features}', show=True)

def plot_P(X, Y, Z, title, fig_path):
	fig = plt.figure()
	ax1 = fig.gca(projection='3d')

	ax1.plot_wireframe(X, Y, Z)
	ax1.set_title(title,
			 horizontalalignment='center',
			 fontsize=16,
			 fontweight='bold')
	ax1.set_xlabel('Generations')
	ax1.set_ylabel('Reward')
	ax1.set_zlabel('Probability')
	ax1.view_init(30, 235)

	plt.tight_layout()
	plt.draw()
	plt.pause(1)
	fig.savefig(fig_path, format='png', dpi=600)
	plt.close()

if __name__ == '__main__':
	print(
		f'alg policySteps found total_time_steps init_steps is% mean_steps ms% mean-init (m-i)% std_steps median_steps'
		f' reward_mean reward_std reward_med avail_iters mean_iters mean_grad_updates')
	for exp_id in [670, 671, 672]:
		alg = "DQN"
		stats_path = dp_dir.joinpath(folder_name, 'logs', f'exp{exp_id}')
		files = [x for x in stats_path.iterdir() if (x.is_file() and x.name.endswith('stats.yml')
													 and alg in x.name)]

		features_to_path = defaultdict(lambda: [])
		for f in files:
			features = f.name.split("_")
			f_name = [features[1], features[9].replace("pt", "")]
			features_to_path[tuple(f_name)] += [f]

		keys = sorted(list(features_to_path.keys()))
		for k in keys:
			v = features_to_path[k]
			k_str = " ".join(k[1:])
			print(f'{alg} {k_str}', end=' ')
			process_all_runs(k, v, buffer_loaded=False)
