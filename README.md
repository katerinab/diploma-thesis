# Hybrid Neuroevolution - Kateřina Brejchová, Diploma Thesis

### Abstract
*"Neuroevolution is an approach for learning artificial neural networks by an evolutionary algorithm. The evolutionary algorithm can evolve both the topology of the network as well as weights and biases. While evolutionary algorithms perform well in exploration, local fine-tuning can be problematic. This thesis proposes a hybrid approach that combines the neuroevolutionary algorithm HyperNEAT with gradient-based algorithm DQN. Firstly, we propose three different options for initialising DQN by a solution found by HyperNEAT. Secondly, we propose a method for fine-tuning HyperNEAT's population by a solution found by DQN. Finally, we combine the two proposed steps into a training loop that iteratively runs the sequence of HyperNEAT and DQN. We test the approach in a reinforcement learning control domain with discrete action space, namely Cart pole, Acrobot and Mountain car environments from OpenAI gym. We conclude that the main challenge in combining the two algorithms is the different interpretability of their outputs. We describe the initialisation strategies that did not work and discuss the possible reasoning behind it. We show promising results for both the DQN and the HyperNEAT initialisation."*

### Project installation
The proposed approach is implemented in Python 3.8 and PyTorch 1.7. The project dependencies are managed by the Conda environment management system. The Conda environment specifications can be found in `configs/environment.yml`. To install the environment run `conda env create --name env-name --file=environment.yml` and then `conda activate env-name`.

All of the tested approaches are implemented with a unified interface in files `dqn.py`, `hyperneat.py`, `hyperneat_dqn_joined.py` and `hyperneat_dqn_loop.py` respectively.


### Code contributions
The code contributions of the thesis are the following: We have designed and implemented the genome representation of CPPN, we implemented converter from the genome to CPPN where CPPN is a fully working PyTorch module. We implemented a converter that creates a feed-forward policy network from the given CPPN and substrate; this policy network is also implemented as a PyTorch module. Implementation of the policy network as a PyTorch module allows easy transfer of the HyperNEAT policy parameters to the DQN policy network. We have implemented buffer initialisation for DQN and subclassed DQN class from Stable Baselines 3 to implement the training loop that uses external policy to guide the search. We have implemented the backpropagation of weights and biases from DQN policy to CPPN network, conversion of the CPPN network back to genome representation and initialisation procedure reproducing the fine-tuned genome into the new population. We have used `python-neat` package for the underlying NEAT implementation.

The algorithm hyperparameters are passed via a command line or a `yaml` configuration file. The hyperparameters are handled by a dataclass defined in `utils/experiment_config.py`. An example interface to run experiments is provided in `experiment.py`. All of the four algorithm scripts also have a main method for local testing, which is a good starting point to test the project.

### Authors

* **Kateřina Brejchová**
* **Ing. Jiří Kubalík, Ph.D.** (thesis supervisor)


See `thesis-text/dp.pdf` for more information about the project.