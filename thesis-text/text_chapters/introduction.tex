\section{Introduction} % Introduction

Neuroevolution is a subfield of artificial intelligence that combines evolutionary algorithms and neural networks. The traditional approach with neural networks is to train them using gradient-based methods such as stochastic gradient descent. This approach has experienced many successes over the recent years in domains such as image classification \cite{krizhevsky_imagenet_2017}, natural language processing \cite{collobert_natural_2011} or recommender systems \cite{bobadilla_recommender_2013}. However, an often mentioned problem is that it takes time and a lot of experience to design the neural network architecture to be efficient, accurate, and learn quickly. Evolutionary algorithms are inspired by natural evolution and work with a population of highly randomised solutions that are gradually recombined and mutated to be pushed towards the optimum. They show success, especially for black-box problems where exploring a suboptimal solution is very valuable and hard to obtain by conventional algorithms. An example of such domains are combinatorial \cite{kobler_evolutionary_2009} or control problems \cite{fleming_evolutionary_2002}.

One of the efficient algorithms combining neural networks and evolutionary algorithms is NEAT \cite{stanley_evolving_2002}. This method represents the networks directly as graphs and evolves them similarly as in genetic programming. The algorithm starts with minimal graph structures and gradually complexifies them as the algorithm progresses. While this method worked for smaller networks, with the growth of the typical network and the emergence of deep learning, it was clear that direct encoding of the networks would not be sufficient due to memory and speed requirements.

Hence, indirect encoding is used to represent the neural networks. This idea is used in a method called HyperNEAT \cite{stanley_hypercube-based_2009}. The encoding is a function called compositional pattern-producing network (CPPN) that, given coordinates of two neurons in the network constrained by a specified substrate (network size and connectivity), generates the weight between them. Then, rather than recombining large neural networks, we recombine more compact functions that generate them. 

We aim to combine HyperNEAT with a gradient-based Deep Q-learning Networks (DQN) algorithm \cite{mnih_playing_2013}. The motivation is that the hybridised solution leverages the exploration capabilities of evolutionary algorithms as well as the fine-tuning capabilities of the gradient-based methods. To accomplish this, we investigate the following possibilities of hybridisation:
\begin{enumerate}
    \item We take the solution found by HyperNEAT and fine-tune it by local search.
    \item We take the fine-tuned model and backpropagate its parameters to the population of generating functions from HyperNEAT.
    \item We iterate these two approaches to obtain a well-performing solution.
\end{enumerate}

We test the baseline HyperNEAT, DQN and the proposed approaches on three classic control problems in the reinforcement learning domain. We use the state-of-the-art DQN algorithm both for our local search extension as well as for the baseline solution. The goal of the experiments is to show the effect of DQN policy initialisation by HyperNEAT and the effect of fine-tuning CPPN(s) based on the weights and biases of the policy network trained by DQN.

The main contributions of this thesis are:
\begin{itemize}
    \item the proposed strategies for initialisation of DQN by HyperNEAT policy and vice-versa,
    \item an experimental evaluation of the hybrid neuroevolution approaches,
    \item a PyTorch-based implementation of the proposed methods.
\end{itemize}

The thesis is divided into the following chapters: Chapter \ref{sec:background} describes the basics of neural networks, evolutionary algorithms and neuroevolution, where we review the relevant state-of-the-art literature. Chapter \ref{sec:rl} describes the tested reinforcement learning domain and relevant algorithms. Chapter \ref{sec:solution} presents the proposed solution, more specifically the combination of HyperNEAT and DQN. Chapter \ref{sec:program} briefly describes the implementation and the used software. Chapter \ref{sec:experiments} provides results from the experimental evaluation of the proposed approaches and discusses the results. Chapter \ref{sec:conclusion} summarises the outcomes of the thesis.