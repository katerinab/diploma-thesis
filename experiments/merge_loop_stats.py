from collections import defaultdict
from pathlib import Path

import matplotlib.pyplot as plt
from utils.hyperneat_reporter import Stats
from utils.plot_ut import plot_all_stats
import numpy as np

FINAL_RES = 'results_final'
SEPARATED = False
dp_dir = Path(__file__).resolve().parent.parent
folder_names = {
	(600, 601, 602): 'results_600_gen',
	(610, 611, 612): 'results_610',
	(660, 661, 662): 'results_660',
	(670, 671, 672): 'results_670',
	(686, 687): 'results_680_new'
}


if __name__ == '__main__':
	for exp_indexes, folder_name in folder_names.items():
		for exp_id in exp_indexes:
			print(f"---------------\n{folder_name} {exp_id}")
			stats_path = dp_dir.joinpath(folder_name, 'logs', f'exp{exp_id}')
			stats_path_merged = dp_dir.joinpath(FINAL_RES, 'logs_merged' + ('' if SEPARATED else '_inter'), f'exp{exp_id}')
			files = [x for x in stats_path.iterdir() if (x.is_file() and x.name.endswith('stats.yml'))]

			features_to_path = defaultdict(lambda: [])
			for f in files:
				features = f.name.split("_")
				f_name = [features[1], features[2].replace("run", ""), features[18] if SEPARATED else '']
				features_to_path[tuple(f_name)] += [f]

			keys = sorted(list(features_to_path.keys()))
			stats_path_merged.mkdir(parents=True, exist_ok=True)
			for k in keys:
				v = sorted(features_to_path[k], key=lambda x: (int(x.name.split("_")[-3].replace("iter","")), 0 if "HyperNEAT" in x.name else 1))
				print(k)
				stats_dict = dict()
				merged_stats = Stats.load(v[0])
				last_t_s, last_gen = merged_stats.generation_stats[-1].time_steps, merged_stats.generation_stats[-1].generation

				# correct best fitness for HyperNEAT
				last_best = merged_stats.generation_stats[0].fitness_best
				for i in range(len(merged_stats.generation_stats)):
					last_best = max(last_best, merged_stats.generation_stats[i].fitness_best)
					merged_stats.generation_stats[i].fitness_best = last_best
					merged_stats.generation_stats[i].time_steps = np.abs(merged_stats.generation_stats[i].time_steps).item()
				# merge the rest of the stats
				for f_id, f in enumerate(v[1:]):
					print(f"\t\t{f.name}")
					stats = Stats.load(f)
					for i in range(len(stats.generation_stats)):
						stats.generation_stats[i].time_steps = np.abs(stats.generation_stats[i].time_steps).item() + last_t_s
						stats.generation_stats[i].generation += last_gen
						last_best = max(last_best, stats.generation_stats[i].fitness_best)
						stats.generation_stats[i].fitness_best = last_best
					last_t_s, last_gen = stats.generation_stats[-1].time_steps, stats.generation_stats[-1].generation
					merged_stats.generation_stats += stats.generation_stats
				new_f = stats_path_merged.joinpath(v[0].name)
				merged_stats.save(new_f)
