import copy
import pickle
import time
from pathlib import Path
from typing import Optional, Union, Tuple
import gym

import numpy as np
from neat.reporting import BaseReporter
from neat.six_util import iteritems
from stable_baselines3.common.vec_env import VecEnv

from algorithms.hyperneat_classes.feed_forward_network import FeedForwardNetwork
from utils.stats import Stats, GenerationStats


def evaluate_policy(
        model: FeedForwardNetwork,
        env: Union[gym.Env, VecEnv],
        n_eval_episodes: int,
        max_episode_length: int,
) -> Tuple[float, float]:
    """
    Adapted from DQN stable baselines 3

    Runs policy for ``n_eval_episodes`` episodes and returns average reward.
    This is made to work only with one env.

    .. note::
        If environment has not been wrapped with ``Monitor`` wrapper, reward and
        episode lengths are counted as it appears with ``env.step`` calls. If
        the environment contains wrappers that modify rewards or episode lengths
        (e.g. reward scaling, early episode reset), these will affect the evaluation
        results as well. You can avoid this by wrapping environment with ``Monitor``
        wrapper before anything else.

    :param model: The RL agent you want to evaluate.
    :param env: The gym environment. In the case of a ``VecEnv``
        this must contain only one environment.
    :param n_eval_episodes: Number of episode to evaluate the agent
    :param reward_threshold: Minimum expected reward per episode,
        this will raise an error if the performance is not met
    :param max_episode_length: Maximum number of time steps in one episodes after which the environment will be interrupted
    and reseted.
    :return: Mean reward per episode, std of reward per episode.
        Returns ([float], [int]) when ``return_episode_rewards`` is True, first
        list containing per-episode rewards and second containing per-episode lengths
        (in number of steps).
    """

    if isinstance(env, VecEnv):
        assert env.num_envs == 1, "You must pass only one environment when using this function"

    episode_rewards, episode_lengths = [], []
    while len(episode_rewards) < n_eval_episodes:
        # Number of loops here might differ from true episodes
        # played, if underlying wrappers modify episode lengths.
        # Avoid double reset, as VecEnv are reset automatically.
        obs = env.reset()
        done = False
        episode_reward = 0
        episode_length = 0
        while not done and episode_length < max_episode_length:
            action = model.predict(obs)
            obs, reward, done, info = env.step(action)
            episode_reward += reward.item()
            episode_length += 1
        else:
            episode_rewards.append(episode_reward)
            episode_lengths.append(episode_length)

    mean_reward = np.mean(episode_rewards)
    std_reward = np.std(episode_rewards)

    return mean_reward.item(), std_reward.item()


class Reporter(BaseReporter):
    def __init__(self, path: Path, num_validation_runs: int, bsf_path: Path):
        super(Reporter, self).__init__()
        self.log_file = path
        self.bsf_path = bsf_path
        self.best_fitness_so_far = -np.inf
        self.generation: Optional[int] = None
        self.generation_start_time: Optional[float] = None
        self.stats: Stats = Stats(generation_stats=[])

        self.num_extinctions: int = 0
        self.most_fit_genomes = []
        self.num_validation_runs = num_validation_runs

    def start_generation(self, generation):
        self.generation = generation
        self.stats.generation_stats += [GenerationStats(species={})]
        self.stats.generation_stats[self.generation].generation = generation
        if len(self.stats.generation_stats) == 1:
            self.stats.generation_stats[self.generation].time_steps = 0
        else:
            self.stats.generation_stats[self.generation].time_steps =\
                self.stats.generation_stats[self.generation - 1].time_steps

        self.generation_start_time = time.time()

    def end_generation(self, config, population, species_set):
        self.stats.generation_stats[self.generation].time_elapsed = time.time() - self.generation_start_time
        # print("---- Generation stats ----")
        # print(self.stats.generation_stats[self.generation].dumps_yaml())
        print(f'gen={self.generation} best(gen)={self.stats.generation_stats[self.generation].fitness_best} '
              f'avg(gen)={self.stats.generation_stats[self.generation].fitness_avg} '
              f'std(gen)={self.stats.generation_stats[self.generation].fitness_std}')
        self.stats.save(self.log_file)

    def post_evaluate(self, config, population, species, best_genome):
        self.most_fit_genomes.append(copy.deepcopy(best_genome))

        for sid, specie in iteritems(species.species):
            self.stats.generation_stats[self.generation].species[sid] = len(specie.members)

        fitnesses = [c.fitness for c in population.values()]

        self.stats.generation_stats[self.generation].fitness_avg = np.mean(fitnesses).item()
        self.stats.generation_stats[self.generation].fitness_std = np.std(fitnesses).item()
        self.stats.generation_stats[self.generation].fitness_best = best_genome.fitness.item()

        if best_genome.fitness.item() >= self.best_fitness_so_far:
            self.best_fitness_so_far = best_genome.fitness.item()
            with open(self.bsf_path, "wb") as f:
                pickle.dump(best_genome, f)

        n_neurons_best, n_conns_best = best_genome.size()
        self.stats.generation_stats[self.generation].n_neurons_best = n_neurons_best
        self.stats.generation_stats[self.generation].n_conns_best = n_conns_best
        self.stats.generation_stats[self.generation].pop_size = len(population)
        self.stats.generation_stats[self.generation].n_species = len(species.species)
        self.stats.generation_stats[self.generation].n_extinctions = self.num_extinctions
        timesteps = np.abs(np.round(self.stats.generation_stats[self.generation].fitness_avg * self.num_validation_runs \
                    * self.stats.generation_stats[self.generation].pop_size)).astype(int).item()
        self.stats.generation_stats[self.generation].time_steps += timesteps

    def get_species_sizes(self):
        all_species = set()
        for gen_data in self.stats.generation_stats:
            all_species = all_species.union(gen_data.species.keys())

        max_species = max(all_species)
        return [
            [gen_data.species[sid] if sid in gen_data.species else 0 for sid in range(1, max_species + 1)]
            for gen_data in self.stats.generation_stats
        ]

    def complete_extinction(self):
        self.num_extinctions += 1

    def found_solution(self, config, generation, best):
        print(f"Solution found in generation {generation}:\n{best}")
        self.end_generation(config, None, None)
        pass

    def species_stagnant(self, sid, species):
        pass
