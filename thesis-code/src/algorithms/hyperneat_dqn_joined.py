import pickle
import random as rnd
from time import time

from stable_baselines3 import DQN
from stable_baselines3.common.buffers import ReplayBuffer
from stable_baselines3.common.save_util import save_to_pkl

from algorithms.algorithm_interface import AlgorithmInterface
from algorithms.dqn import Dqn
from algorithms.hyperneat import HyperNEAT
from algorithms.neat_extensions.multigenome import MultiGenome
from algorithms.neat_extensions.multireproduction import MultiReproduction
from algorithms.hyperneat_classes.feed_forward_network import FeedForwardNetwork
from algorithms.hyperneat_classes.genome_handler import GenomeType
from utils.dqn_reporter import evaluate_policy as evaluate_dqn
from utils.experiment_config import ExperimentConfig
from utils.hyperneat_reporter import evaluate_policy as evaluate_hyperneat
from utils.plot_ut import *


class JoinedHyperneatDqn(AlgorithmInterface):
    """
    Combination of HyperNEAT and DQN.

    :param main_config: Experiment config
    :param code_dir: Directory where the results are saved
    :param experiment_name: Name of the experiment, serves as identifier to save the results
    :param run_id: Id of the run to distinguish between different runs with the same hyper parameters
    :param max_reward: Reward where the environment is considered to be solved
    """
    def __init__(self, main_config: ExperimentConfig, code_dir: Path, experiment_name: str, run_id: int,
                 max_reward: int):
        super().__init__(experiment_cfg=main_config, code_dir=code_dir, exp_name=experiment_name, run_id=run_id)
        self.neat_cfg = neat.Config(
            MultiGenome, MultiReproduction, neat.DefaultSpeciesSet, neat.DefaultStagnation, self.neat_config_path,
        )
        self.neat_cfg.genome_config.set_cppn_type(self.exp_cfg.genome_type)
        self.exp_cfg = main_config

        # load environment details
        env = self.make_env()
        self.max_reward = max_reward
        self.n_actions = env.action_space.n
        self.n_observations = env.observation_space.shape[0]
        self.n_generations = self.exp_cfg.max_train_iterations / self.exp_cfg.pop_size
        assert self.exp_cfg.pop_size == self.neat_cfg.pop_size, "Population size must be the same as defined in NEAT config"
        # stats + logger
        self.logger: Reporter

    def save_configs(self):
        self.exp_cfg.save(self.save_experiment_path)
        self.neat_cfg.save(self.save_neat_config_path)

    def save_final_result(self, best_results: Tuple[MultiGenome, DQN]):
        final_genome, final_dqn_model = best_results
        final_dqn_model.save(self.solution_path)
        with open(self.midsolution_path, "wb") as f:
            pickle.dump(final_genome, f)
        with open(self.popsolution_path, "wb") as f:
            pickle.dump(self.alg1_hyperneat.pop, f)

    def create_and_save_replay_buffer(self, final_net: FeedForwardNetwork):
        """
        Use the provided network to fill the replay buffer to certain size with random epsilon.

        :param final_net: The network to generate action
        """
        replay_buffer = ReplayBuffer(
            self.alg2_dqn.model.buffer_size,
            self.alg2_dqn.model.observation_space,
            self.alg2_dqn.model.action_space,
            self.alg2_dqn.model.device,
            optimize_memory_usage=self.alg2_dqn.model.optimize_memory_usage,
        )

        num_collected_steps = 0

        env = self.get_vec_env(self.exp_cfg.max_episode_length)
        while num_collected_steps < self.exp_cfg.prefilled_buffer_size:
            done = False
            obs = env.reset()
            self.alg2_dqn.model._last_obs = obs
            while not done and num_collected_steps < self.exp_cfg.prefilled_buffer_size:
                # Select action randomly or according to policy
                if rnd.random() < self.exp_cfg.replay_buffer_epsilon:
                    action = np.array([env.action_space.sample()])
                else:
                    action = final_net.predict(obs)
                new_obs, reward, done, infos = env.step(action)
                num_collected_steps += 1
                self.alg2_dqn.model._store_transition(replay_buffer, action, new_obs, reward, done, infos)
                obs = new_obs

        save_to_pkl(self.replay_buffer_path, replay_buffer)

    def train(self) -> Tuple[Tuple[MultiGenome, DQN], Tuple[Tuple[float, float], Tuple[float, float]]]:
        """
        Train HyperNEAT, initialize DQN, train DQN and return the best results from both of the algorithms.
        :return: best results
        """
        # Algorithms init
        self.alg1_hyperneat = HyperNEAT(self.exp_cfg, self.code_dir, self.experiment_name, self.run_id)
        self.alg1_hyperneat.neat_cfg.fitness_threshold = self.max_reward
        self.alg2_dqn = Dqn(self.exp_cfg, self.code_dir, self.experiment_name, self.run_id, self.max_reward)

        # train HyperNEAT
        (best_genome, _), best_fitness = self.alg1_hyperneat.train()
        best_genome = self.alg1_hyperneat.load_best_solution()
        final_net = self.alg1_hyperneat.make_net(best_genome)

        # set initial solution to dqn
        state_dict = self.load_dqn_like_state_dict_from_hyperneat_net(final_net,
                                                                      full_update=self.exp_cfg.initialize_both_q_nets)
        if self.exp_cfg.load_replay_buffer:
            self.create_and_save_replay_buffer(final_net)
        if self.exp_cfg.use_external_policy:
            self.alg2_dqn.model.external_policy = final_net
        self.alg2_dqn.load_solution(initial_solution=state_dict, force_full_update=self.exp_cfg.initialize_both_q_nets,
                                    replay_buffer_path=self.replay_buffer_path if self.exp_cfg.load_replay_buffer else None)

        ####################################################### DOUBLE check fitness
        fitness_hyperneat = evaluate_hyperneat(final_net,
                                               env=self.get_vec_env(seed=self.exp_cfg.seed,
                                                                    max_episode_steps=self.exp_cfg.max_episode_length),
                                               n_eval_episodes=self.exp_cfg.max_test_iterations,
                                               max_episode_length=self.exp_cfg.max_episode_length)
        fitness_dqn = evaluate_dqn(self.alg2_dqn.model,
                                   env=self.get_vec_env(seed=self.exp_cfg.seed,
                                                        max_episode_steps=self.exp_cfg.max_episode_length),
                                   n_eval_episodes=self.exp_cfg.max_test_iterations,
                                   max_episode_length=self.exp_cfg.max_episode_length)
        print(
            f"Fitness of HyperNEAT model: {fitness_hyperneat=}\nFitness of the same model loaded to DQN {fitness_dqn=}")

        #############################  Train DQN + eval  ################################################
        self.alg2_dqn.train()
        print(f"Original fitness: {fitness_hyperneat}")
        fitness_dqn = evaluate_dqn(self.alg2_dqn.model,
                                   env=self.get_vec_env(seed=self.exp_cfg.seed,
                                                        max_episode_steps=self.exp_cfg.max_episode_length),
                                   n_eval_episodes=self.exp_cfg.max_test_iterations,
                                   max_episode_length=self.exp_cfg.max_episode_length)
        print(f"DQN fitness after training: {fitness_dqn}")

        return (best_genome, self.alg2_dqn.model), (fitness_hyperneat, fitness_dqn)

    @staticmethod
    def load_dqn_like_state_dict_from_hyperneat_net(final_net, full_update):
        """
        Transfer state dict to DQN-like state dict.
        :param final_net: The network parameters
        :param full_update: True if Q net should be initialized as well, if False, initializes only Q net target
        :return: the DQN-like state dict
        """
        new_state_dict = {}
        for key in final_net.state_dict().keys():
            value = final_net.state_dict()[key]
            if full_update:
                q_key = key.replace("layers", "q_net.q_net")
                new_state_dict[q_key] = value
            qt_key = key.replace("layers", "q_net_target.q_net")
            new_state_dict[qt_key] = value
        return new_state_dict

    def plot_results(self, models, exp_id, duration, device):
        """
        Plot results of both of the algorithms.
        :param models: models
        :param exp_id: experiment ida
        :param duration: duration of the experiment
        :param device: device for tensors
        """
        (best_genome, final_model) = models
        self.alg1_hyperneat.plot_results((best_genome, None), exp_id, duration, device)
        self.alg2_dqn.plot_results((None, final_model), exp_id, duration, device)


if __name__ == '__main__':
    env_name, max_reward = ('CartPole-v1', 195)
    code_dir = Path(__file__).resolve().parent.parent.parent
    config_path = code_dir.joinpath('configs', 'config.yml')
    SEEDS = [260679797, 701623387, 882840679, 157005113, 871591532, 609579736, 73563671, 312938638, 180649226,
             403804907]

    exp_id = 34
    for run_id in range(2, 3):
        experiment_config = ExperimentConfig.create(alg_name='Joined_HyperNEAT_DQN', cfg_path=config_path,
                                                    env_name=env_name, seed=SEEDS[run_id], device='cuda',
                                                    gen_type=GenomeType.W_B_2, gamma=0.9, target_update_frequency=10000,
                                                    replay_buffer_epsilon=0.2, init_q_net=False)
        experiment_config.prefilled_buffer_size = 50000
        experiment_config.use_external_policy = True
        experiment_config.external_policy_threshold_time_steps = 100000
        start = time()

        print(f'Running experiment {env_name=}')
        exp_name = f'exp{exp_id:03}_{experiment_config.algorithm_name}_{env_name}_{experiment_config.genome_type.value}_run{run_id}'

        algorithm = JoinedHyperneatDqn(experiment_config, code_dir, exp_name, run_id, max_reward)

        (best_genome, final_model), best_fitness = algorithm.train()
        print(
            f"\n\n\n--------------------\nFinal performance: {best_fitness}")
        run_duration = time() - start
        algorithm.plot_results(models=(best_genome, final_model), exp_id=exp_id, duration=run_duration,
                               device=experiment_config.device)
        algorithm.save((best_genome, final_model))
        print(f'Experiment {exp_name} run for {run_duration} s which is {run_duration / 60} min {run_duration % 60} s')
