from simple_parsing import ArgumentParser

from algorithms.dqn import Dqn
from algorithms.hyperneat import HyperNEAT
from algorithms.hyperneat_dqn_joined import JoinedHyperneatDqn
from algorithms.hyperneat_dqn_loop import HyperNeatDqnLoop
from utils.experiment_config import ExperimentConfig

from utils.plot_ut import *
from time import time


code_dir = Path(__file__).resolve().parent.parent
config_path = code_dir.joinpath('configs', 'config.yml')
env_to_max_reward = {'CartPole-v0': 195,
				'Acrobot-v1': -60, # estimate from leaderboard
				'MountainCar-v0': -110,
                }

parser = ArgumentParser()
parser.add_arguments(ExperimentConfig, dest="experiment_config")

args = parser.parse_args()
experiment_config: ExperimentConfig = args.experiment_config
max_reward = env_to_max_reward[experiment_config.environment]

start = time()
print(f'Running experiment {experiment_config.environment=} {experiment_config.exp_id=} {experiment_config.run_id=}')
exp_name = f'exp{experiment_config.exp_id:03}_{experiment_config.environment}_run{experiment_config.run_id}' \
           f'_buffer{experiment_config.prefilled_buffer_size}_rollout{experiment_config.dqn_num_rollouts}' \
           f'_buferEps{experiment_config.replay_buffer_epsilon}'\
           f'_batch{experiment_config.dqn_batch_size}_qNetInit{experiment_config.init_q_net}_qNetType{experiment_config.initialize_both_q_nets}' \
           f'_pt{experiment_config.external_policy_threshold_time_steps}_loadBuffer{experiment_config.load_replay_buffer}' \
           f'_trainIters{experiment_config.max_train_iterations}_targetUpdateFreq{experiment_config.target_update_frequency}' \
           f'_useExtPolicy{experiment_config.use_external_policy}_genome{experiment_config.genome_type.value}'\
           f'_q{experiment_config.cppn_backprop_quality_threshold}_iter0'
print(experiment_config.dumps_yaml())

algorithm = None
print(experiment_config.algorithm_name)
if experiment_config.algorithm_name == "DQN":
    print("Running DQN")
    assert experiment_config.use_external_policy is False, "Cannot use external policy for baseline DQN"
    assert experiment_config.init_q_net is False, "Cannot use initialize qNet for baseline DQN"
    algorithm = Dqn(experiment_config, code_dir, exp_name, experiment_config.run_id, max_reward=max_reward)
elif experiment_config.algorithm_name == "HyperNEAT":
    print("Running HyperNEAT")
    algorithm = HyperNEAT(experiment_config, code_dir, exp_name, experiment_config.run_id)
    algorithm.neat_cfg.fitness_threshold = max_reward
elif experiment_config.algorithm_name == "Joined_HyperNEAT_DQN":
    print("Running HyperNEAT->DQN joined")
    algorithm = JoinedHyperneatDqn(experiment_config, code_dir, exp_name, experiment_config.run_id, max_reward=max_reward)
    algorithm.neat_cfg.fitness_threshold = max_reward
elif experiment_config.algorithm_name == "HyperNEAT_DQN_loop":
    print("Running HyperNEAT->DQN loop")
    algorithm = HyperNeatDqnLoop(experiment_config, code_dir, exp_name, experiment_config.run_id, max_reward=max_reward)
    algorithm.neat_cfg.fitness_threshold = max_reward
else:
    print("Incorrect algorithm type")
    exit(-1)


(best_genome, final_model), best_fitness = algorithm.train()

print(f"\n\n\n--------------------\nFinal performance: {best_fitness}")
run_duration = time() - start
if experiment_config.algorithm_name != "HyperNEAT_DQN_loop":
    algorithm.plot_results(models=(best_genome, final_model), exp_id=experiment_config.exp_id, duration=run_duration,
                           device=experiment_config.device)
    algorithm.save((best_genome, final_model))
print(f'Experiment {exp_name} run for {run_duration} s which is {run_duration / 60} min {run_duration % 60} s')

